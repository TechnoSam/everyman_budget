from setuptools import setup, find_packages

VERSION = '0.2.0'
DESCRIPTION = 'Everyman Budget'
LONG_DESCRIPTION = 'A library that defines the database operations for the Everyman Budget'

setup(
    name='everyman_budget',
    version=VERSION,
    author='Samuel McFalls',
    author_email='technosam4jc@gmail.com',
    description=DESCRIPTION,
    long_description=LONG_DESCRIPTION,
    packages=find_packages(),
    install_requires=['SQLAlchemy', 'alembic', 'colorlog', 'psycopg2'],

    keywords=['python', 'budget', 'everyman'],
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Programming Language :: Python :: 3',
        'Operating System :: POSIX :: Linux'
        'Operating System :: MacOS :: MacOS X',
        'Operating System :: Microsoft :: Windows',
    ]
)
