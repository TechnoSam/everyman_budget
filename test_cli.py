import logging
import sys

import colorlog

from budget.database.database_manager_2p0p0 import DatabaseManager2p0p0

logger = logging.getLogger()


def setup_logging():
    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s %(levelname)-8s | %(message)s | %(module)s:%(lineno)d')
    colored_formatter = colorlog.ColoredFormatter(
        '%(asctime)s %(log_color)s%(levelname)-8s%(reset)s | %(log_color)s%(message)s%(reset)s | %(module)s:%(lineno)d',
        log_colors={
            'DEBUG': 'bold_cyan',
            'INFO': 'bold_green',
            'WARNING': 'bold_yellow',
            'ERROR': 'bold_red',
            'CRITICAL': 'bold_red,bg_bold_white',
        },
    )

    stream_handler = logging.StreamHandler()
    stream_handler.setStream(sys.stderr)  # Logging is diagnostic info and should go to stderr
    stream_handler.setLevel(logging.DEBUG)
    stream_handler.setFormatter(colored_formatter if sys.stderr.isatty() else formatter)
    logger.addHandler(stream_handler)

    logger.debug('Debug message')
    logger.info('Info message')
    logger.warning('Warning message')
    logger.error('Error message')
    logger.critical('Critical message')


if __name__ == '__main__':
    setup_logging()
    logger.info('Starting application')
    d = DatabaseManager2p0p0('test.db')

