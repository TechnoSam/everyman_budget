import logging
import sys

import colorlog
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine

from everyman_budget.database.action import *
from everyman_budget.config import Config
from everyman_budget.initialize import is_initialized

logger = logging.getLogger()


def setup_logging():
    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s %(levelname)-8s | %(message)s | %(name)s:%(lineno)d')
    colored_formatter = colorlog.ColoredFormatter(
        '%(asctime)s %(log_color)s%(levelname)-8s%(reset)s | %(log_color)s%(message)s%(reset)s | %(name)s:%(lineno)d',
        log_colors={
            'DEBUG': 'bold_cyan',
            'INFO': 'bold_green',
            'WARNING': 'bold_yellow',
            'ERROR': 'bold_red',
            'CRITICAL': 'bold_red,bg_bold_white',
        },
    )

    stream_handler = logging.StreamHandler()
    stream_handler.setStream(sys.stderr)  # Logging is diagnostic info and should go to stderr
    stream_handler.setLevel(logging.DEBUG)
    stream_handler.setFormatter(colored_formatter if sys.stderr.isatty() else formatter)
    logger.addHandler(stream_handler)

    logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)
    logging.getLogger('alembic').setLevel(logging.INFO)
    logging.getLogger('budget.database.action').setLevel(logging.DEBUG)

    logger.debug('Debug message')
    logger.info('Info message')
    logger.warning('Warning message')
    logger.error('Error message')
    logger.critical('Critical message')


if __name__ == '__main__':
    setup_logging()
    Session = sessionmaker()

    if not is_initialized():
        logger.critical('Database is not initialized, please run initialize.py')
        exit(1)

    config = Config()
    engine = create_engine(config.url)
    connection = engine.connect()

    trans = connection.begin()
    session = Session(bind=connection)

    try:
        bank = add_account(session, 'Bank')
        rent = add_account(session, 'Rent')
        fun = add_account(session, 'Fun')

        job = add_party(session, 'Job')
        landlord = add_party(session, 'Landlord')
        cfa = add_party(session, 'Chick-fil-A')
        micro_center = add_party(session, 'Micro Center')
        amazon = add_party(session, 'Amazon')
        visa = add_party(session, 'Visa')
        shell = add_party(session, 'Shell')
        session.commit()

        add_transaction(session, bank, Decimal('1000'), datetime(2020, 1, 1), party=job, notes='Paycheck #1')
        transfer(session, rent, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Rent')
        transfer(session, fun, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Fun')
        add_transaction(session, fun, Decimal('-7.82'), datetime(2020, 1, 2), party=cfa, notes='Celebrate new job')
        add_transaction(session, rent, Decimal('-498'), datetime(2020, 1, 5), party=landlord)
        add_transaction(session, fun, Decimal('-20.00'), datetime(2020, 1, 10), party=shell, notes='Gas')
        add_transaction(session, fun, Decimal('-24.22'), datetime(2020, 1, 10), party=cfa, notes='Party')
        test = add_transaction(session, fun, Decimal('-129.99'), datetime(2020, 1, 10), party=micro_center,
                               notes='New Keyboard')
        add_transaction(session, bank, Decimal('1000'), datetime(2020, 1, 14), party=job, notes='Paycheck #2')
        transfer(session, fun, bank, Decimal('1000'), datetime(2020, 1, 14))
        one = add_transaction(session, fun, Decimal('-499.99'), datetime(2020, 1, 18), party=micro_center,
                              notes='New graphics card')
        two = add_transaction(session, fun, Decimal('-279.99'), datetime(2020, 1, 19), party=amazon,
                              notes='New processor')
        add_group(session, datetime(2020, 2, 1), name='Payment CC091234', party=visa,
                  transactions=[one, two], notes='Paid off credit card')
        session.commit()

        account_balances = [
            (fun, Decimal('537.99')),
            (fun, Decimal('817.98')),
            (fun, Decimal('1317.97')),
            (bank, Decimal('0')),
            (bank, Decimal('1000')),
            (fun, Decimal('317.97')),
            (fun, Decimal('447.96')),
            (fun, Decimal('472.18')),
            (rent, Decimal('2')),
            (fun, Decimal('492.18')),
            (fun, Decimal('500')),
            (bank, Decimal('0')),
            (rent, Decimal('500')),
            (bank, Decimal('500')),
            (bank, Decimal('1000')),
        ]
        transactions = get_transactions(session)

        move_transaction(session, test, datetime(2020, 1, 10), new_order=0)
        session.commit()

        account_balances = [
            (fun, Decimal('537.99')),
            (fun, Decimal('817.98')),
            (fun, Decimal('1317.97')),
            (bank, Decimal('0')),
            (bank, Decimal('1000')),
            (fun, Decimal('317.97')),
            (fun, Decimal('342.19')),
            (fun, Decimal('362.19')),
            (rent, Decimal('2')),
            (fun, Decimal('492.18')),
            (fun, Decimal('500')),
            (bank, Decimal('0')),
            (rent, Decimal('500')),
            (bank, Decimal('500')),
            (bank, Decimal('1000')),
        ]
        transactions = get_transactions(session)
        session.commit()
    except Exception as e:
        print(e)
    finally:
        session.close()
        trans.rollback()
        connection.close()
