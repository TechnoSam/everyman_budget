# Everyman Budget
This Python library provides an interface to a database that stores transaction and budget information. The structure
supports an application organization that is simple enough that anyone could use it, not just accountants and tech
people. Thus, Everyman Budget. The budget is zero-based, cash-in-envelope style budgeting, inspired by Dave Ramsey's
Baby Steps system.

## Setup

Install PostgreSQL and create the database. Refer to the
[official documentation](https://www.postgresql.org/download/). Also consider using a
[Docker container](https://hub.docker.com/_/postgres). (Only PostgreSQL is supported)

Set the required environment variables. (See the [Config](#config) section).

Run `everyman_budget/initialize.py` to create the schema and initialize migrations.

## Library
The `everyman_budget` package provides the python library. It is tested with Python 3.8.2.

The library is not available on PyPI yet, I'm not sure if I'm ready for that. For now, build a `wheel` by running
`setup.py`.

```bash
$ python setup.py bdist_wheel

```

If you don't already have it, you'll need to install `wheel`.

```bash
$ python -m pip install wheel
```

Then you can install the library locally.

```bash
$ python -m pip install dist/everyman_budget-0.2.0-py3-none-any.whl 
```

### Config

Configuration is handled through environment variables. The following variables must be set or any application using the
library will exit.

|Variable |Explanation | Sample Value|
--- | --- | ---
|BUDGET_DB_HOST|The host that the database server is running on|127.0.0.1|
|BUDGET_DB_NAME|The name of the database to use|mcfalls_family_budget|
|BUDGET_DB_USERNAME|The username to log into the database with|smcfalls|
|BUDGET_DB_PASSWORD|The password to log into the database with|CorrectHorseBatteryStaple|

### API Documentation

Coming soon.

Everything is under the `everyman_budget` module.

### Tests

The `everyman_budget.action` module maintains tests with the `unittest` module.
To run all tests, use `python -m unittest`. You can hide the test output with `python -m unittest -b`.

You will need the database running to run the tests. In that sense it may not technically be unit tests,
but it's close enough for me.

### Logging

`everyman_budget` uses the standard Python logging module.

## Database

Only PostgreSQL is supported. Tested with version 13.0.
