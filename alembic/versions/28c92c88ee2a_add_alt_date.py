"""add alt date

Revision ID: 28c92c88ee2a
Revises: 
Create Date: 2020-12-16 15:39:29.009717

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '28c92c88ee2a'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('transaction', sa.Column('alt_date', sa.Date))


def downgrade():
    op.drop_column('transaction', 'alt_date')
