from sqlalchemy import Column, Integer, String, Boolean, Numeric, Date, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Account(Base):
    __tablename__ = 'account'

    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True, nullable=False)
    hide = Column(Boolean, nullable=False)
    color = Column(String(6))

    transactions = relationship('Transaction', back_populates='account')

    def __repr__(self):
        return "<Account(name='{name}', hide='{hide}', color='{color}')>".format(
            name=self.name, hide=self.hide, color=self.color
        )


class Party(Base):
    __tablename__ = 'party'

    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True, nullable=False)

    transactions = relationship('Transaction', back_populates='party')
    groups = relationship('Group', back_populates='party')

    def __repr__(self):
        return "<Party(name='{name}')>".format(name=self.name)


class Transaction(Base):
    __tablename__ = 'transaction'

    id = Column(Integer, primary_key=True)

    account_id = Column(Integer, ForeignKey('account.id'), nullable=False)
    account = relationship('Account', back_populates='transactions')

    charge = Column(Numeric(19, 4), nullable=False)

    party_id = Column(Integer, ForeignKey('party.id'))
    party = relationship('Party', back_populates='transactions')

    balance = Column(Numeric(19, 4), nullable=False)
    date = Column(Date, nullable=False)
    alt_date = Column(Date)
    order = Column(Integer, nullable=False)
    notes = Column(String)

    parent_id = Column(Integer, ForeignKey('group.id'))
    parent = relationship('Group', back_populates='child_transactions')

    tag_maps = relationship('TagMap', back_populates='transaction')

    def __repr__(self):
        return "<Transaction(account='{account}', charge='{charge}', party='{party}', balance='{balance}'," \
               " date='{date}', alt_date='{alt_date}', order='{order}', notes='{notes}'>"\
            .format(account=self.account, charge=self.charge, party=self.party, balance=self.balance,
                    date=self.date, alt_date=self.alt_date, order=self.order, notes=self.notes)


class Group(Base):
    __tablename__ = 'group'

    id = Column(Integer, primary_key=True)
    name = Column(String)

    party_id = Column(Integer, ForeignKey('party.id'))
    party = relationship('Party', back_populates='groups')

    date = Column(Date, nullable=False)
    notes = Column(String)

    parent_id = Column(Integer, ForeignKey('group.id'))
    parent = relationship('Group', back_populates='child_groups')

    child_transactions = relationship('Transaction', back_populates='parent')

    # I might not need to back populate here. This key is self referential, so it already has the actual parent.
    # Will need to test
    child_groups = relationship('Group')

    tag_group_maps = relationship('TagGroupMap', back_populates='group')


class Tag(Base):
    __tablename__ = 'tag'

    id = Column(Integer, primary_key=True)
    name = Column(String)

    tag_maps = relationship('TagMap')
    tag_group_maps = relationship('TagGroupMap')


class TagMap(Base):
    __tablename__ = 'tag_map'

    id = Column(Integer, primary_key=True)

    tag_id = Column(Integer, ForeignKey('tag.id'), nullable=False)
    tag = relationship('Tag', back_populates='tag_maps')

    transaction_id = Column(Integer, ForeignKey('transaction.id'), nullable=False)
    transaction = relationship('Transaction', back_populates='tag_maps')


class TagGroupMap(Base):
    __tablename__ = 'tag_group_map'

    id = Column(Integer, primary_key=True)

    tag_id = Column(Integer, ForeignKey('tag.id'), nullable=False)
    tag = relationship('Tag', back_populates='tag_group_maps')

    group_id = Column(Integer, ForeignKey('group.id'), nullable=False)
    group = relationship('Group', back_populates='tag_group_maps')
