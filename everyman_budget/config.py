import os
import logging

USERNAME_VAR_NAME = 'BUDGET_DB_USERNAME'
PASSWORD_VAR_NAME = 'BUDGET_DB_PASSWORD'
HOST_VAR_NAME = 'BUDGET_DB_HOST'
DB_NAME_VAR_NAME = 'BUDGET_DB_NAME'

logger = logging.getLogger(__name__)


class ConfigError(RuntimeError):
    pass


class Config:
    def __init__(self):
        if USERNAME_VAR_NAME not in os.environ:
            logger.error('Environment variable {var} is not set'.format(var=USERNAME_VAR_NAME))
            raise ConfigError('Environment variable {var} is not set. '
                              'Refer to the documentation to read about required environment variables.'
                              .format(var=USERNAME_VAR_NAME))
        self.username = os.environ[USERNAME_VAR_NAME]

        if PASSWORD_VAR_NAME not in os.environ:
            logger.error('Environment variable {var} is not set'.format(var=PASSWORD_VAR_NAME))
            raise ConfigError('Environment variable {var} is not set. '
                              'Refer to the documentation to read about required environment variables.'
                              .format(var=PASSWORD_VAR_NAME))
        self.password = os.environ[PASSWORD_VAR_NAME]

        if HOST_VAR_NAME not in os.environ:
            logger.error('Environment variable {var} is not set'.format(var=HOST_VAR_NAME))
            raise ConfigError('Environment variable {var} is not set. '
                              'Refer to the documentation to read about required environment variables.'
                              .format(var=HOST_VAR_NAME))
        self.host = os.environ[HOST_VAR_NAME]

        if DB_NAME_VAR_NAME not in os.environ:
            logger.error('Environment variable {var} is not set'.format(var=DB_NAME_VAR_NAME))
            raise ConfigError('Environment variable {var} is not set. '
                              'Refer to the documentation to read about required environment variables.'
                              .format(var=DB_NAME_VAR_NAME))
        self.db_name = os.environ[DB_NAME_VAR_NAME]

        self.url = 'postgresql://{username}:{password}@{host}/{db_name}'\
            .format(username=self.username, password=self.password, host=self.host, db_name=self.db_name)
        self.url_redacted = 'postgresql://{username}:********@{host}/{db_name}'\
            .format(username=self.username, host=self.host, db_name=self.db_name)
