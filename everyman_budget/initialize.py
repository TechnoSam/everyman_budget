import logging
from pathlib import Path
import sys
import argparse
import os

import colorlog
from sqlalchemy import create_engine
from sqlalchemy.exc import OperationalError
from alembic.config import Config
from alembic import command
from alembic.migration import MigrationContext

from everyman_budget.database.schema import Base
import everyman_budget.config

DEFAULT_ALEMBIC_DIR = Path(__file__).resolve().parent.parent

logger = logging.getLogger(__name__)


def get_current_database_revision():
    logger.info('Getting current database revision')

    try:
        budget_config = everyman_budget.config.Config()
    except everyman_budget.config.ConfigError as e:
        print(e)
        logger.critical(e)
        exit(1)

    logger.info('Connecting to {url}'.format(url=budget_config.url_redacted))
    engine = create_engine(budget_config.url)
    try:
        connection = engine.connect()
    except OperationalError as e:
        error_text = 'Failed to connect to database: {e}'.format(e=str(e))
        print(error_text)
        logger.critical(error_text)
        exit(1)
    context = MigrationContext.configure(connection)
    current_rev = context.get_current_revision()
    logger.debug('Current database revision: {rev}'.format(rev=current_rev))
    return current_rev


def is_initialized():
    logger.info('Checking if the database is initialized')
    return get_current_database_revision() is not None


def initialize(alembic_dir=DEFAULT_ALEMBIC_DIR):
    logger.info('Initializing database')
    if is_initialized():
        raise RuntimeError('Cannot initialize non-empty database. Please use alembic to migrate.')

    # No need to check for an exception, we couldn't know we were uninitialized if the config wasn't right
    budget_config = everyman_budget.config.Config()

    logger.info('Connecting to {url}'.format(url=budget_config.url_redacted))
    engine = create_engine(budget_config.url)

    Base.metadata.create_all(engine)

    os.chdir(alembic_dir)
    alembic_cfg = Config('alembic.ini')
    alembic_cfg.attributes['configure_logger'] = False

    command.stamp(alembic_cfg, 'head')

    logger.info('Initialized database')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Initialize the Everyman Budget Database')
    parser.add_argument('--log-file', default='init.log', help='File to log to')
    parser.add_argument('--log-console', action='store_true', help='Also log to the console')
    parser.add_argument('--alembic-dir', help='The directory to run alembic commands from')

    args = parser.parse_args()

    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s %(levelname)-8s | %(message)s | %(name)s:%(lineno)d')
    colored_formatter = colorlog.ColoredFormatter(
        '%(asctime)s %(log_color)s%(levelname)-8s%(reset)s | %(log_color)s%(message)s%(reset)s | %(name)s:%(lineno)d',
        log_colors={
            'DEBUG': 'bold_cyan',
            'INFO': 'bold_green',
            'WARNING': 'bold_yellow',
            'ERROR': 'bold_red',
            'CRITICAL': 'bold_red,bg_bold_white',
        },
    )

    if args.log_console:
        stream_handler = logging.StreamHandler()
        stream_handler.setStream(sys.stderr)  # Logging is diagnostic info and should go to stderr
        stream_handler.setLevel(logging.DEBUG)
        stream_handler.setFormatter(colored_formatter if sys.stderr.isatty() else formatter)
        logger.addHandler(stream_handler)

    file_handler = logging.FileHandler(args.log_file)
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)

    logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)
    logging.getLogger('alembic').setLevel(logging.INFO)
    try:
        if args.alembic_dir is None:
            initialize()
        else:
            initialize(args.alembic_dir)
    except RuntimeError as e:
        print(e)
        exit(1)
