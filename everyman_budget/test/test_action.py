import unittest
from datetime import datetime
from pathlib import Path

from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine, func

from everyman_budget.database.action import *
from everyman_budget.config import Config
from everyman_budget.initialize import initialize

Session = sessionmaker()

try:
    initialize(alembic_dir=Path(__file__).resolve().parent.parent.parent)
except RuntimeError:
    pass

config = Config()
engine = create_engine(config.url)


class TestAction(unittest.TestCase):
    def setUp(self) -> None:
        self.connection = engine.connect()
        self.trans = self.connection.begin()
        self.session = Session(bind=self.connection)

    def tearDown(self) -> None:
        self.session.close()
        self.trans.rollback()
        self.connection.close()

    def test_add_account(self):
        """Adds an account. Verifies that the account appears in the database and the properties are correct."""
        bank = add_account(self.session, 'Bank', hide=True, color='AA00AA')
        self.session.commit()
        self.assertEqual(bank.name, 'Bank')
        self.assertEqual(bank.hide, True)
        self.assertEqual(bank.color, 'AA00AA')
        test = get_account_by_id(self.session, bank.id)
        self.assertEqual(bank, test)

    def test_find_account(self):
        """Adds several accounts. Verifies that each can be looked up by its name with the correct constraints.

        For now, this means it only searches at the beginning of the name.
        """
        bank = add_account(self.session, 'Bank')

        acc1 = add_account(self.session, 'acc1')
        acc2 = add_account(self.session, 'acc2')
        acc3 = add_account(self.session, 'acc3')

        unicode = add_account(self.session, 'happy 😊 day')

        search = find_account_by_name(self.session, 'b')
        self.assertEqual(len(search), 1)
        self.assertEqual(bank, search[0])

        search = find_account_by_name(self.session, 'acc')
        self.assertEqual(len(search), 3)
        self.assertTrue(acc1 in search)
        self.assertTrue(acc2 in search)
        self.assertTrue(acc3 in search)

        search = find_account_by_name(self.session, 'happy 😊')
        self.assertEqual(len(search), 1)
        self.assertEqual(unicode, search[0])

    def test_get_accounts(self):
        """Adds several accounts. Verifies that all can be fetched."""
        one = add_account(self.session, "one")
        two = add_account(self.session, "two")
        three = add_account(self.session, "three", hide=True)
        self.session.commit()

        test = get_accounts(self.session)
        self.assertEqual(len(test), 3)
        self.assertTrue(one in test)
        self.assertTrue(two in test)
        self.assertTrue(three in test)

    def test_add_party(self):
        """Adds a party. Verifies that the party appears in the database and the properties are correct."""
        cfa = add_party(self.session, 'Chick-fil-A')
        self.session.commit()
        self.assertEqual(cfa.name, 'Chick-fil-A')
        test = get_party_by_id(self.session, cfa.id)
        self.assertEqual(cfa, test)

    def test_find_party(self):
        """Adds several parties. Verifies that each can be looked up by its name with the correct constraints.

        For now, this means it only searches at the beginning of the name.
        """
        bank = add_party(self.session, 'Chick-fil-A')

        par1 = add_party(self.session, 'par1')
        par2 = add_party(self.session, 'par2')
        par3 = add_party(self.session, 'par3')

        unicode = add_party(self.session, 'happy 😊 day')

        search = find_party_by_name(self.session, 'c')
        self.assertEqual(len(search), 1)
        self.assertEqual(bank, search[0])

        search = find_party_by_name(self.session, 'par')
        self.assertEqual(len(search), 3)
        self.assertTrue(par1 in search)
        self.assertTrue(par2 in search)
        self.assertTrue(par3 in search)

        search = find_party_by_name(self.session, 'happy 😊')
        self.assertEqual(len(search), 1)
        self.assertEqual(unicode, search[0])

    def test_add_transaction(self):
        """Adds an account, party, and a transaction.

        Verifies that the transaction is present in the database and has the correct properties.
        """
        bank = add_account(self.session, 'Bank')
        cfa = add_party(self.session, 'Chick-fil-A')
        self.session.commit()

        charge = Decimal('123.45')
        date = datetime(2020, 1, 2)
        alt_date = datetime(2020, 1, 1)

        tx = add_transaction(self.session, bank, charge, date, alt_date=alt_date, party=cfa, notes='1 notes')
        self.session.commit()

        self.assertEqual(tx.account, bank)
        self.assertEqual(tx.charge, charge)
        self.assertEqual(tx.date.year, date.year)
        self.assertEqual(tx.date.month, date.month)
        self.assertEqual(tx.date.day, date.day)
        self.assertEqual(tx.alt_date.year, alt_date.year)
        self.assertEqual(tx.alt_date.month, alt_date.month)
        self.assertEqual(tx.alt_date.day, alt_date.day)
        self.assertEqual(tx.party, cfa)
        self.assertEqual(tx.notes, '1 notes')

        test = get_transaction_by_id(self.session, tx.id)
        self.assertEqual(tx, test)

    def test_add_transaction_lose_precision(self):
        """Adds a transaction with a charge that has more precision than the database can store.

        Verifies that this generates an error.

        (Not implemented)
        """
        pass

    def test_find_transactions(self):
        """Simulates a small sample of a transaction log.

        Verifies that the correct transactions can be found with different filtering rules.
        """
        bank = add_account(self.session, 'Bank')
        rent = add_account(self.session, 'Rent')
        fun = add_account(self.session, 'Fun')

        job = add_party(self.session, 'Job')
        landlord = add_party(self.session, 'Landlord')
        cfa = add_party(self.session, 'Chick-fil-A')
        micro_center = add_party(self.session, 'Micro Center')
        self.session.commit()

        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 1), party=job, notes='Paycheck #1')
        transfer(self.session, rent, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Rent')
        transfer(self.session, fun, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Fun')
        add_transaction(self.session, fun, Decimal('-7.82'), datetime(2020, 1, 2), party=cfa, notes='Celebrate new job')
        add_transaction(self.session, rent, Decimal('-498'), datetime(2020, 1, 5), party=landlord)
        add_transaction(self.session, fun, Decimal('-129.99'), datetime(2020, 1, 10),
                        party=micro_center, notes='New Keyboard')
        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 14), party=job, notes='Paycheck #2')
        self.session.commit()

        test = get_transactions(self.session)
        self.assertEqual(9, len(test))

        test = get_transactions(self.session, accounts=[bank])
        self.assertEqual(4, len(test))

        test = get_transactions(self.session, accounts=[bank, rent])
        self.assertEqual(6, len(test))

        test = get_transactions(self.session, charge_begin=Decimal('500'))
        self.assertEqual(6, len(test))

        test = get_transactions(self.session, charge_begin=Decimal('490'))
        self.assertEqual(7, len(test))

        test = get_transactions(self.session, charge_begin=Decimal('129.99'), charge_end=Decimal('129.99'))
        self.assertEqual(1, len(test))

        test = get_transactions(self.session, charge_begin=Decimal('400'), charge_end=Decimal('500'))
        self.assertEqual(5, len(test))

        test = get_transactions(self.session, charge_end=Decimal('500'))
        self.assertEqual(7, len(test))

        test = get_transactions(self.session, charge_end=Decimal('499.9999'))
        self.assertEqual(3, len(test))

        test = get_transactions(self.session, parties=[job])
        self.assertEqual(2, len(test))

        test = get_transactions(self.session, parties=[job, cfa, landlord])
        self.assertEqual(4, len(test))

        test = get_transactions(self.session, date_begin=datetime(2019, 12, 31))
        self.assertEqual(9, len(test))

        test = get_transactions(self.session, date_begin=datetime(2020, 1, 1))
        self.assertEqual(9, len(test))

        test = get_transactions(self.session, date_begin=datetime(2020, 1, 2))
        self.assertEqual(4, len(test))

        test = get_transactions(self.session, date_begin=datetime(2020, 1, 2), date_end=datetime(2020, 1, 10))
        self.assertEqual(3, len(test))

        test = get_transactions(self.session, date_end=datetime(2020, 1, 10))
        self.assertEqual(8, len(test))

        test = get_transactions(self.session, debit=True)
        self.assertEqual(5, len(test))

        test = get_transactions(self.session, debit=False)
        self.assertEqual(4, len(test))

        test = get_transactions(self.session, notes_contain='new')
        self.assertEqual(2, len(test))

        test = get_transactions(self.session,
                                accounts=[fun, rent],
                                charge_begin=Decimal('100'), charge_end=Decimal('200'),
                                parties=[micro_center, cfa, landlord],
                                date_begin=datetime(2020, 1, 2), date_end=datetime(2020, 1, 12),
                                debit=True,
                                notes_contain='new')
        self.assertEqual(1, len(test))
        self.assertEqual(fun, test[0].account)
        self.assertEqual(Decimal('-129.99'), test[0].charge)
        self.assertEqual(micro_center, test[0].party)
        self.assertEqual(2020, test[0].date.year)
        self.assertEqual(1, test[0].date.month)
        self.assertEqual(10, test[0].date.day)
        self.assertEqual('New Keyboard', test[0].notes)

    def test_add_group(self):
        """Simulates a small sample of a transaction log with a flat group.

        Verifies that the group is present in the database and has the correct properties, including charge.

        """
        bank = add_account(self.session, 'Bank')
        rent = add_account(self.session, 'Rent')
        fun = add_account(self.session, 'Fun')

        job = add_party(self.session, 'Job')
        landlord = add_party(self.session, 'Landlord')
        cfa = add_party(self.session, 'Chick-fil-A')
        micro_center = add_party(self.session, 'Micro Center')
        amazon = add_party(self.session, 'Amazon')
        visa = add_party(self.session, 'Visa')
        self.session.commit()

        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 1), party=job, notes='Paycheck #1')
        transfer(self.session, rent, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Rent')
        transfer(self.session, fun, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Fun')
        add_transaction(self.session, fun, Decimal('-7.82'), datetime(2020, 1, 2), party=cfa, notes='Celebrate new job')
        add_transaction(self.session, rent, Decimal('-498'), datetime(2020, 1, 5), party=landlord)
        add_transaction(self.session, fun, Decimal('-129.99'), datetime(2020, 1, 10), party=micro_center,
                        notes='New Keyboard')
        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 14), party=job, notes='Paycheck #2')
        transfer(self.session, fun, bank, Decimal('1000'), datetime(2020, 1, 14))
        one = add_transaction(self.session, fun, Decimal('-499.99'), datetime(2020, 1, 18),
                              party=micro_center, notes='New graphics card')
        two = add_transaction(self.session, fun, Decimal('-279.99'), datetime(2020, 1, 19),
                              party=amazon, notes='New processor')
        group = add_group(self.session, datetime(2020, 2, 1), name='Payment CC091234', party=visa,
                          transactions=[one, two], notes='Paid off credit card')
        self.session.commit()

        self.assertEqual(2, len(group.child_transactions))
        self.assertTrue(one in group.child_transactions)
        self.assertTrue(two in group.child_transactions)
        self.assertEqual(0, len(group.child_groups))
        self.assertEqual('Payment CC091234', group.name)
        self.assertEqual(visa, group.party)
        self.assertEqual('Paid off credit card', group.notes)
        self.assertEqual(Decimal('-779.98'), get_group_charge(group))

        test = get_group_by_id(self.session, group.id)
        self.assertEqual(group, test)

    def test_find_group(self):
        """Simulates a small sample of a transaction log with multiple flat groups.

        Verifies that they can be found by searching the database.
        """
        bank = add_account(self.session, 'Bank')
        rent = add_account(self.session, 'Rent')
        fun = add_account(self.session, 'Fun')

        job = add_party(self.session, 'Job')
        landlord = add_party(self.session, 'Landlord')
        cfa = add_party(self.session, 'Chick-fil-A')
        micro_center = add_party(self.session, 'Micro Center')
        amazon = add_party(self.session, 'Amazon')
        visa = add_party(self.session, 'Visa')
        mastercard = add_party(self.session, 'Mastercard')
        apple = add_party(self.session, 'Apple')
        apple_card = add_party(self.session, 'Apple Card')
        shell = add_party(self.session, 'Shell')
        self.session.commit()

        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 1), party=job, notes='Paycheck #1')
        transfer(self.session, rent, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Rent')
        transfer(self.session, fun, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Fun')
        add_transaction(self.session, fun, Decimal('-7.82'), datetime(2020, 1, 2), party=cfa, notes='Celebrate new job')
        add_transaction(self.session, rent, Decimal('-498'), datetime(2020, 1, 5), party=landlord)
        add_transaction(self.session, fun, Decimal('-129.99'), datetime(2020, 1, 10), party=micro_center,
                        notes='New Keyboard')
        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 14), party=job, notes='Paycheck #2')
        transfer(self.session, fun, bank, Decimal('1000'), datetime(2020, 1, 14))
        one = add_transaction(self.session, fun, Decimal('-499.99'), datetime(2020, 1, 18),
                              party=micro_center, notes='New graphics card')
        two = add_transaction(self.session, fun, Decimal('-279.99'), datetime(2020, 1, 19),
                              party=amazon, notes='New processor')
        group_1 = add_group(self.session, datetime(2020, 2, 1), name='Payment CC091234', party=visa,
                            transactions=[one, two], notes='Group 1')
        three = add_transaction(self.session, fun, Decimal('-10.99'), datetime(2020, 2, 3),
                                party=amazon, notes='Stuff 1')
        four = add_transaction(self.session, fun, Decimal('-14.99'), datetime(2020, 2, 3),
                               party=amazon, notes='Stuff 2')
        group_2 = add_group(self.session, datetime(2020, 2, 3), party=mastercard,
                            transactions=[three, four], notes='Group 2')
        five = add_transaction(self.session, fun, Decimal('-839.22'), datetime(2020, 2, 5),
                               party=apple, notes='Stuff 3')
        six = add_transaction(self.session, fun, Decimal('-39.99'), datetime(2020, 2, 7),
                              party=apple, notes='Stuff 4')
        seven = add_transaction(self.session, fun, Decimal('-20'), datetime(2020, 2, 8),
                                party=shell, notes='Stuff 5')
        group_3 = add_group(self.session, datetime(2020, 2, 10), party=apple_card,
                            transactions=[five, six, seven], notes='Group 3')
        self.session.commit()

        test = get_groups(self.session)
        self.assertEqual(6, len(test))

        test = get_groups(self.session, charge_begin=Decimal('50'))
        self.assertEqual(2, len(test))
        self.assertIn(group_1, test)
        self.assertIn(group_3, test)

        test = get_groups(self.session, charge_begin=Decimal('50'), charge_end=Decimal('800'))
        self.assertEqual(1, len(test))
        self.assertIn(group_1, test)

        test = get_groups(self.session, charge_end=Decimal('780'))
        self.assertEqual(5, len(test))
        self.assertIn(group_1, test)
        self.assertIn(group_2, test)
        self.assertNotIn(group_3, test)

        test = get_groups(self.session, date_begin=datetime(2020, 2, 1))
        self.assertEqual(3, len(test))
        self.assertIn(group_1, test)
        self.assertIn(group_2, test)
        self.assertIn(group_3, test)

        test = get_groups(self.session, date_begin=datetime(2020, 2, 1), date_end=datetime(2020, 2, 8))
        self.assertEqual(2, len(test))
        self.assertIn(group_1, test)
        self.assertIn(group_2, test)

        test = get_groups(self.session, date_end=datetime(2020, 2, 7))
        self.assertEqual(5, len(test))
        self.assertIn(group_1, test)
        self.assertIn(group_2, test)
        self.assertNotIn(group_3, test)

        test = get_groups(self.session, parties=[apple_card])
        self.assertEqual(1, len(test))
        self.assertIn(group_3, test)

        test = get_groups(self.session, parties=[apple_card, visa])
        self.assertEqual(2, len(test))
        self.assertIn(group_3, test)
        self.assertIn(group_1, test)

        test = get_groups(self.session, notes_contain='Budget')
        self.assertEqual(2, len(test))
        self.assertNotIn(group_1, test)
        self.assertNotIn(group_2, test)
        self.assertNotIn(group_3, test)

        test = get_groups(self.session, notes_contain='Group')
        self.assertEqual(3, len(test))
        self.assertIn(group_1, test)
        self.assertIn(group_2, test)
        self.assertIn(group_3, test)

        test = get_groups(self.session, notes_contain='Group 2')
        self.assertEqual(1, len(test))
        self.assertIn(group_2, test)

        test = get_groups(self.session, charge_begin=Decimal('500'), charge_end=Decimal('800'), parties=[visa],
                          date_begin=datetime(2020, 2, 1), date_end=datetime(2020, 2, 8), notes_contain='Group')
        self.assertEqual(1, len(test))
        self.assertIn(group_1, test)

    def test_add_nested_group(self):
        """Simulates a small sample of a transaction log with a nested group.

        Verifies that the group is present in the database and has the correct properties, including charge.
        """
        bank = add_account(self.session, 'Bank')
        rent = add_account(self.session, 'Rent')
        fun = add_account(self.session, 'Fun')
        dining = add_account(self.session, 'Dining')

        job = add_party(self.session, 'Job')
        landlord = add_party(self.session, 'Landlord')
        cfa = add_party(self.session, 'Chick-fil-A')
        visa = add_party(self.session, 'Visa')
        self.session.commit()

        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 1), party=job, notes='Paycheck #1')
        transfer(self.session, rent, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Rent')
        transfer(self.session, fun, bank, Decimal('400'), datetime(2020, 1, 1), notes='Budget for Fun')
        transfer(self.session, dining, bank, Decimal('100'), datetime(2020, 1, 1), notes='Budget for Dining')
        one = add_transaction(self.session, rent, Decimal('-498'), datetime(2020, 1, 5), party=landlord)
        two = add_transaction(self.session, dining, Decimal('-7.89'), datetime(2020, 1, 7), party=cfa, notes='Meal')
        three = add_transaction(self.session, fun, Decimal('-2.99'), datetime(2020, 1, 7), party=cfa, notes='Dessert')
        group_1 = add_group(self.session, datetime(2020, 1, 7), name='Out to CFA', party=cfa,
                            transactions=[two, three], notes='Test 1')
        group_2 = add_group(self.session, datetime(2020, 1, 15), name='Auto-pay', party=visa,
                            sub_groups=[group_1], transactions=[one], notes='Test 2')
        self.session.commit()

        self.assertEqual(1, len(group_2.child_transactions))
        self.assertTrue(one in group_2.child_transactions)
        self.assertEqual(1, len(group_2.child_groups))
        self.assertEqual('Auto-pay', group_2.name)
        self.assertEqual(visa, group_2.party)
        self.assertEqual('Test 2', group_2.notes)

        self.assertEqual(group_1, group_2.child_groups[0])
        self.assertEqual(2, len(group_2.child_groups[0].child_transactions))
        self.assertTrue(two in group_2.child_groups[0].child_transactions)
        self.assertTrue(three in group_2.child_groups[0].child_transactions)
        self.assertEqual(0, len(group_2.child_groups[0].child_groups))
        self.assertEqual('Out to CFA', group_2.child_groups[0].name)
        self.assertEqual(cfa, group_2.child_groups[0].party)
        self.assertEqual('Test 1', group_2.child_groups[0].notes)
        self.assertEqual(Decimal('-10.88'), get_group_charge(group_2.child_groups[0]))

        self.assertEqual(Decimal('-508.88'), get_group_charge(group_2))

        test = get_group_by_id(self.session, group_2.id)
        self.assertEqual(group_2, test)

    def test_get_account_balance(self):
        """Simulates a small sample of a transaction log.

        Verifies that all account balances are consistent throughout the entire history.
        """
        bank = add_account(self.session, 'Bank')
        rent = add_account(self.session, 'Rent')
        fun = add_account(self.session, 'Fun')

        job = add_party(self.session, 'Job')
        landlord = add_party(self.session, 'Landlord')
        cfa = add_party(self.session, 'Chick-fil-A')
        micro_center = add_party(self.session, 'Micro Center')
        amazon = add_party(self.session, 'Amazon')
        visa = add_party(self.session, 'Visa')
        self.session.commit()

        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 1), party=job, notes='Paycheck #1')
        transfer(self.session, rent, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Rent')
        transfer(self.session, fun, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Fun')
        add_transaction(self.session, fun, Decimal('-7.82'), datetime(2020, 1, 2), party=cfa, notes='Celebrate new job')
        add_transaction(self.session, rent, Decimal('-498'), datetime(2020, 1, 5), party=landlord)
        add_transaction(self.session, fun, Decimal('-129.99'), datetime(2020, 1, 10), party=micro_center,
                        notes='New Keyboard')
        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 14), party=job, notes='Paycheck #2')
        transfer(self.session, fun, bank, Decimal('1000'), datetime(2020, 1, 14))
        one = add_transaction(self.session, fun, Decimal('-499.99'), datetime(2020, 1, 18), party=micro_center,
                              notes='New graphics card')
        two = add_transaction(self.session, fun, Decimal('-279.99'), datetime(2020, 1, 19), party=amazon,
                              notes='New processor')
        add_group(self.session, datetime(2020, 2, 1), name='Payment CC091234', party=visa,
                  transactions=[one, two], notes='Paid off credit card')
        self.session.commit()

        self.assertEqual(Decimal('0'), get_account_balance(self.session, bank, date=datetime(2019, 12, 31)))
        self.assertEqual(Decimal('0'), get_account_balance(self.session, bank, date=datetime(2020, 1, 1)))
        self.assertEqual(Decimal('0'), get_account_balance(self.session, bank, date=datetime(2020, 1, 2)))
        self.assertEqual(Decimal('0'), get_account_balance(self.session, bank, date=datetime(2020, 1, 5)))
        self.assertEqual(Decimal('0'), get_account_balance(self.session, bank, date=datetime(2020, 1, 10)))
        self.assertEqual(Decimal('0'), get_account_balance(self.session, bank, date=datetime(2020, 1, 14)))
        self.assertEqual(Decimal('0'), get_account_balance(self.session, bank, date=datetime(2020, 1, 18)))
        self.assertEqual(Decimal('0'), get_account_balance(self.session, bank, date=datetime(2020, 1, 19)))

        self.assertEqual(Decimal('0'),       get_account_balance(self.session, fun, date=datetime(2019, 12, 31)))
        self.assertEqual(Decimal('500'),     get_account_balance(self.session, fun, date=datetime(2020, 1, 1)))
        self.assertEqual(Decimal('492.18'),  get_account_balance(self.session, fun, date=datetime(2020, 1, 2)))
        self.assertEqual(Decimal('492.18'),  get_account_balance(self.session, fun, date=datetime(2020, 1, 5)))
        self.assertEqual(Decimal('362.19'),  get_account_balance(self.session, fun, date=datetime(2020, 1, 10)))
        self.assertEqual(Decimal('1362.19'), get_account_balance(self.session, fun, date=datetime(2020, 1, 14)))
        self.assertEqual(Decimal('862.20'),  get_account_balance(self.session, fun, date=datetime(2020, 1, 18)))
        self.assertEqual(Decimal('582.21'),  get_account_balance(self.session, fun, date=datetime(2020, 1, 19)))

        self.assertEqual(Decimal('0'),   get_account_balance(self.session, rent, date=datetime(2019, 12, 31)))
        self.assertEqual(Decimal('500'), get_account_balance(self.session, rent, date=datetime(2020, 1, 1)))
        self.assertEqual(Decimal('500'), get_account_balance(self.session, rent, date=datetime(2020, 1, 2)))
        self.assertEqual(Decimal('2'),   get_account_balance(self.session, rent, date=datetime(2020, 1, 5)))
        self.assertEqual(Decimal('2'),   get_account_balance(self.session, rent, date=datetime(2020, 1, 10)))
        self.assertEqual(Decimal('2'),   get_account_balance(self.session, rent, date=datetime(2020, 1, 14)))
        self.assertEqual(Decimal('2'),   get_account_balance(self.session, rent, date=datetime(2020, 1, 18)))
        self.assertEqual(Decimal('2'),   get_account_balance(self.session, rent, date=datetime(2020, 1, 19)))

        self.assertEqual(Decimal('1000'), get_account_balance(self.session, bank, date=datetime(2020, 1, 1), order=0))
        self.assertEqual(Decimal('500'), get_account_balance(self.session, bank, date=datetime(2020, 1, 1), order=2))
        self.assertEqual(Decimal('0'), get_account_balance(self.session, bank, date=datetime(2020, 1, 1), order=4))

    def test_transfer(self):
        """Simulates several transfers between accounts.

        Verifies that all accounts have the correct balance.
        """
        bank = add_account(self.session, 'Bank')
        rent = add_account(self.session, 'Rent')
        fun = add_account(self.session, 'Fun')

        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 1))
        transfer(self.session, rent, bank, Decimal('600'), datetime(2020, 1, 1))
        transfer(self.session, fun, bank, Decimal('500'), datetime(2020, 1, 1))

        self.assertEqual(Decimal('-100'), get_account_balance(self.session, bank))
        self.assertEqual(Decimal('600'), get_account_balance(self.session, rent))
        self.assertEqual(Decimal('500'), get_account_balance(self.session, fun))

    def test_edit_account(self):
        """Simulates a small sample of a transaction log, then edits an account's properties.

        Verifies that the account changes in the database and is reflected in the transactions.
        """
        bank = add_account(self.session, 'Bank')
        rent = add_account(self.session, 'Rent')
        fun = add_account(self.session, 'Fun', hide=False, color='FF8C00')

        job = add_party(self.session, 'Job')
        landlord = add_party(self.session, 'Landlord')
        cfa = add_party(self.session, 'Chick-fil-A')
        micro_center = add_party(self.session, 'Micro Center')
        amazon = add_party(self.session, 'Amazon')
        self.session.commit()

        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 1), party=job, notes='Paycheck #1')
        transfer(self.session, rent, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Rent')
        transfer(self.session, fun, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Fun')
        test_1 = add_transaction(self.session, fun, Decimal('-7.82'), datetime(2020, 1, 2), party=cfa,
                                 notes='Celebrate new job')
        add_transaction(self.session, rent, Decimal('-498'), datetime(2020, 1, 5), party=landlord)
        test_2 = add_transaction(self.session, fun, Decimal('-129.99'), datetime(2020, 1, 10), party=micro_center,
                                 notes='New Keyboard')
        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 14), party=job, notes='Paycheck #2')
        transfer(self.session, fun, bank, Decimal('1000'), datetime(2020, 1, 14))
        add_transaction(self.session, fun, Decimal('-499.99'), datetime(2020, 1, 18), party=micro_center,
                        notes='New graphics card')
        test_3 = add_transaction(self.session, fun, Decimal('-279.99'), datetime(2020, 1, 19), party=amazon,
                                 notes='New processor')
        self.session.commit()

        self.assertEqual('Fun', fun.name)
        self.assertEqual(False, fun.hide)
        self.assertEqual('FF8C00', fun.color)

        test = get_account_by_id(self.session, fun.id)
        self.assertEqual(fun, test)
        self.assertEqual('Fun', test.name)
        self.assertEqual(False, test.hide)
        self.assertEqual('FF8C00', test.color)

        test = test_1.account
        self.assertEqual(fun, test)
        self.assertEqual('Fun', test.name)
        self.assertEqual(False, test.hide)
        self.assertEqual('FF8C00', test.color)

        test = test_2.account
        self.assertEqual(fun, test)
        self.assertEqual('Fun', test.name)
        self.assertEqual(False, test.hide)
        self.assertEqual('FF8C00', test.color)

        test = test_3.account
        self.assertEqual(fun, test)
        self.assertEqual('Fun', test.name)
        self.assertEqual(False, test.hide)
        self.assertEqual('FF8C00', test.color)

        edit_account(fun, name='Fun Rename', hide=True, color='000000')

        self.assertEqual('Fun Rename', fun.name)
        self.assertEqual(True, fun.hide)
        self.assertEqual('000000', fun.color)

        test = get_account_by_id(self.session, fun.id)
        self.assertEqual(fun, test)
        self.assertEqual('Fun Rename', test.name)
        self.assertEqual(True, test.hide)
        self.assertEqual('000000', test.color)

        test = test_1.account
        self.assertEqual(fun, test)
        self.assertEqual('Fun Rename', test.name)
        self.assertEqual(True, test.hide)
        self.assertEqual('000000', test.color)

        test = test_2.account
        self.assertEqual(fun, test)
        self.assertEqual('Fun Rename', test.name)
        self.assertEqual(True, test.hide)
        self.assertEqual('000000', test.color)

        test = test_3.account
        self.assertEqual(fun, test)
        self.assertEqual('Fun Rename', test.name)
        self.assertEqual(True, test.hide)
        self.assertEqual('000000', test.color)

    def test_edit_party(self):
        """Simulates a small sample of a transaction log, then edits a party's properties.

        Verifies that the party changes in the database and is reflected in the transactions.
        """
        bank = add_account(self.session, 'Bank')
        rent = add_account(self.session, 'Rent')
        fun = add_account(self.session, 'Fun', hide=False, color='FF8C00')

        job = add_party(self.session, 'Job')
        landlord = add_party(self.session, 'Landlord')
        cfa = add_party(self.session, 'Chick-fil-A')
        micro_center = add_party(self.session, 'Micro Center')
        amazon = add_party(self.session, 'Amazon')
        self.session.commit()

        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 1), party=job, notes='Paycheck #1')
        transfer(self.session, rent, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Rent')
        transfer(self.session, fun, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Fun')
        add_transaction(self.session, fun, Decimal('-7.82'), datetime(2020, 1, 2), party=cfa,
                        notes='Celebrate new job')
        add_transaction(self.session, rent, Decimal('-498'), datetime(2020, 1, 5), party=landlord)
        test_1 = add_transaction(self.session, fun, Decimal('-129.99'), datetime(2020, 1, 10), party=micro_center,
                                 notes='New Keyboard')
        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 14), party=job, notes='Paycheck #2')
        transfer(self.session, fun, bank, Decimal('1000'), datetime(2020, 1, 14))
        test_2 = add_transaction(self.session, fun, Decimal('-499.99'), datetime(2020, 1, 18), party=micro_center,
                                 notes='New graphics card')
        add_transaction(self.session, fun, Decimal('-279.99'), datetime(2020, 1, 19), party=amazon,
                        notes='New processor')
        test_3 = add_transaction(self.session, fun, Decimal('44.99'), datetime(2020, 1, 19), party=micro_center,
                                 notes='New mouse')
        self.session.commit()

        self.assertEqual('Micro Center', micro_center.name)

        test = get_party_by_id(self.session, micro_center.id)
        self.assertEqual('Micro Center', test.name)

        test = test_1.party
        self.assertEqual('Micro Center', test.name)

        test = test_2.party
        self.assertEqual('Micro Center', test.name)

        test = test_3.party
        self.assertEqual('Micro Center', test.name)

        edit_party(micro_center, name='Micro Center Rename')

        self.assertEqual('Micro Center Rename', micro_center.name)

        test = get_party_by_id(self.session, micro_center.id)
        self.assertEqual('Micro Center Rename', test.name)

        test = test_1.party
        self.assertEqual('Micro Center Rename', test.name)

        test = test_2.party
        self.assertEqual('Micro Center Rename', test.name)

        test = test_3.party
        self.assertEqual('Micro Center Rename', test.name)

    def test_edit_transaction_trivial(self):
        """Simulates a small sample of a transaction log, then edits a transaction's trivial properties,
        i.e. properties that only effect the transaction and not future transactions,
        such as notes, party, and alt_date.

        Verifies that the transaction changes in the database.
        """
        bank = add_account(self.session, 'Bank')
        rent = add_account(self.session, 'Rent')
        fun = add_account(self.session, 'Fun', hide=False, color='FF8C00')

        job = add_party(self.session, 'Job')
        landlord = add_party(self.session, 'Landlord')
        cfa = add_party(self.session, 'Chick-fil-A')
        micro_center = add_party(self.session, 'Micro Center')
        amazon = add_party(self.session, 'Amazon')
        self.session.commit()

        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 1), party=job, notes='Paycheck #1')
        transfer(self.session, rent, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Rent')
        transfer(self.session, fun, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Fun')
        add_transaction(self.session, fun, Decimal('-7.82'), datetime(2020, 1, 2), party=cfa,
                        notes='Celebrate new job')
        add_transaction(self.session, rent, Decimal('-498'), datetime(2020, 1, 5), party=landlord)
        test = add_transaction(self.session, fun, Decimal('-129.99'), datetime(2020, 1, 10), party=micro_center,
                               notes='New Keyboard')
        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 14), party=job, notes='Paycheck #2')
        transfer(self.session, fun, bank, Decimal('1000'), datetime(2020, 1, 14))
        add_transaction(self.session, fun, Decimal('-499.99'), datetime(2020, 1, 18), party=micro_center,
                        notes='New graphics card')
        add_transaction(self.session, fun, Decimal('-279.99'), datetime(2020, 1, 19), party=amazon,
                        notes='New processor')
        self.session.commit()

        self.assertEqual('New Keyboard', test.notes)
        self.assertEqual(micro_center, test.party)
        self.assertEqual(None, test.alt_date)

        edit_transaction_notes(test, 'Accident')
        edit_transaction_party(test, amazon)
        edit_transaction_alt_date(test, datetime(2020, 1, 9))
        self.session.commit()

        self.assertEqual('Accident', test.notes)
        self.assertEqual(amazon, test.party)
        self.assertEqual(2020, test.alt_date.year)
        self.assertEqual(1, test.alt_date.month)
        self.assertEqual(9, test.alt_date.day)

        test_2 = get_transactions(self.session, charge_begin=Decimal('129.99'), charge_end=Decimal('129.99'))
        self.assertEqual(1, len(test_2))
        test_2 = test_2[0]

        self.assertEqual(test, test_2)
        self.assertEqual('Accident', test_2.notes)
        self.assertEqual(amazon, test_2.party)
        self.assertEqual(2020, test_2.alt_date.year)
        self.assertEqual(1, test_2.alt_date.month)
        self.assertEqual(9, test_2.alt_date.day)

        edit_transaction_notes(test, None)
        edit_transaction_party(test, None)
        self.session.commit()

        self.assertEqual(None, test.notes)
        self.assertEqual(None, test.party)

        test_2 = get_transactions(self.session, charge_begin=Decimal('129.99'), charge_end=Decimal('129.99'))
        self.assertEqual(1, len(test_2))
        test_2 = test_2[0]

        self.assertEqual(test, test_2)
        self.assertEqual(None, test_2.notes)
        self.assertEqual(None, test_2.party)

    def test_edit_transaction_charge(self):
        """Simulates a small sample of a transaction log, then edits a transaction's charge.

        Verifies that the transaction changes in the database and is reflected in the future transactions.
        """
        bank = add_account(self.session, 'Bank')
        rent = add_account(self.session, 'Rent')
        fun = add_account(self.session, 'Fun')

        job = add_party(self.session, 'Job')
        landlord = add_party(self.session, 'Landlord')
        cfa = add_party(self.session, 'Chick-fil-A')
        micro_center = add_party(self.session, 'Micro Center')
        amazon = add_party(self.session, 'Amazon')
        visa = add_party(self.session, 'Visa')
        self.session.commit()

        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 1), party=job, notes='Paycheck #1')
        transfer(self.session, rent, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Rent')
        transfer(self.session, fun, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Fun')
        add_transaction(self.session, fun, Decimal('-7.82'), datetime(2020, 1, 2), party=cfa, notes='Celebrate new job')
        add_transaction(self.session, rent, Decimal('-498'), datetime(2020, 1, 5), party=landlord)
        test = add_transaction(self.session, fun, Decimal('-129.99'), datetime(2020, 1, 10), party=micro_center,
                               notes='New Keyboard')
        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 14), party=job, notes='Paycheck #2')
        transfer(self.session, fun, bank, Decimal('1000'), datetime(2020, 1, 14))
        one = add_transaction(self.session, fun, Decimal('-499.99'), datetime(2020, 1, 18), party=micro_center,
                              notes='New graphics card')
        two = add_transaction(self.session, fun, Decimal('-279.99'), datetime(2020, 1, 19), party=amazon,
                              notes='New processor')
        add_group(self.session, datetime(2020, 2, 1), name='Payment CC091234', party=visa,
                  transactions=[one, two], notes='Paid off credit card')
        self.session.commit()

        account_balances = [
            (fun, Decimal('582.21')),
            (fun, Decimal('862.20')),
            (fun, Decimal('1362.19')),
            (bank, Decimal('0')),
            (bank, Decimal('1000')),
            (fun, Decimal('362.19')),
            (rent, Decimal('2')),
            (fun, Decimal('492.18')),
            (fun, Decimal('500')),
            (bank, Decimal('0')),
            (rent, Decimal('500')),
            (bank, Decimal('500')),
            (bank, Decimal('1000')),
        ]
        transactions = get_transactions(self.session)
        for (balance, transaction) in zip(account_balances, transactions):
            self.assertEqual(balance, (transaction.account, transaction.balance))

        edit_transaction_charge(self.session, test, Decimal('-149.99'))
        self.session.commit()

        account_balances = [
            (fun, Decimal('562.21')),
            (fun, Decimal('842.20')),
            (fun, Decimal('1342.19')),
            (bank, Decimal('0')),
            (bank, Decimal('1000')),
            (fun, Decimal('342.19')),
            (rent, Decimal('2')),
            (fun, Decimal('492.18')),
            (fun, Decimal('500')),
            (bank, Decimal('0')),
            (rent, Decimal('500')),
            (bank, Decimal('500')),
            (bank, Decimal('1000')),
        ]
        transactions = get_transactions(self.session)
        for (balance, transaction) in zip(account_balances, transactions):
            self.assertEqual(balance, (transaction.account, transaction.balance))

    def test_edit_transaction_account(self):
        """Simulates a small sample of a transaction log, then edits a transaction's account.

        Verifies that the transaction changes in the database and is reflected in the future transactions.
        """
        bank = add_account(self.session, 'Bank')
        rent = add_account(self.session, 'Rent')
        fun = add_account(self.session, 'Fun')

        job = add_party(self.session, 'Job')
        landlord = add_party(self.session, 'Landlord')
        cfa = add_party(self.session, 'Chick-fil-A')
        micro_center = add_party(self.session, 'Micro Center')
        amazon = add_party(self.session, 'Amazon')
        visa = add_party(self.session, 'Visa')
        self.session.commit()

        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 1), party=job, notes='Paycheck #1')
        transfer(self.session, rent, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Rent')
        transfer(self.session, fun, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Fun')
        add_transaction(self.session, fun, Decimal('-7.82'), datetime(2020, 1, 2), party=cfa, notes='Celebrate new job')
        add_transaction(self.session, rent, Decimal('-498'), datetime(2020, 1, 5), party=landlord)
        test = add_transaction(self.session, fun, Decimal('-129.99'), datetime(2020, 1, 10), party=micro_center,
                               notes='New Keyboard')
        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 14), party=job, notes='Paycheck #2')
        transfer(self.session, fun, bank, Decimal('1000'), datetime(2020, 1, 14))
        one = add_transaction(self.session, fun, Decimal('-499.99'), datetime(2020, 1, 18), party=micro_center,
                              notes='New graphics card')
        two = add_transaction(self.session, fun, Decimal('-279.99'), datetime(2020, 1, 19), party=amazon,
                              notes='New processor')
        add_group(self.session, datetime(2020, 2, 1), name='Payment CC091234', party=visa,
                  transactions=[one, two], notes='Paid off credit card')
        self.session.commit()

        account_balances = [
            (fun, Decimal('582.21')),
            (fun, Decimal('862.20')),
            (fun, Decimal('1362.19')),
            (bank, Decimal('0')),
            (bank, Decimal('1000')),
            (fun, Decimal('362.19')),
            (rent, Decimal('2')),
            (fun, Decimal('492.18')),
            (fun, Decimal('500')),
            (bank, Decimal('0')),
            (rent, Decimal('500')),
            (bank, Decimal('500')),
            (bank, Decimal('1000')),
        ]
        transactions = get_transactions(self.session)
        for (balance, transaction) in zip(account_balances, transactions):
            self.assertEqual(balance, (transaction.account, transaction.balance))

        edit_transaction_account(self.session, test, rent)
        self.session.commit()

        account_balances = [
            (fun, Decimal('712.20')),
            (fun, Decimal('992.19')),
            (fun, Decimal('1492.18')),
            (bank, Decimal('0')),
            (bank, Decimal('1000')),
            (rent, Decimal('-127.99')),
            (rent, Decimal('2')),
            (fun, Decimal('492.18')),
            (fun, Decimal('500')),
            (bank, Decimal('0')),
            (rent, Decimal('500')),
            (bank, Decimal('500')),
            (bank, Decimal('1000')),
        ]
        transactions = get_transactions(self.session)
        for (balance, transaction) in zip(account_balances, transactions):
            self.assertEqual(balance, (transaction.account, transaction.balance))

    def test_move_transaction(self):
        """Simulates a small sample of a transaction log, then moves a transaction by editing the date.

        Verifies that the transaction changes in the database and is reflected in the surrounding transactions.
        """
        bank = add_account(self.session, 'Bank')
        rent = add_account(self.session, 'Rent')
        fun = add_account(self.session, 'Fun')

        job = add_party(self.session, 'Job')
        landlord = add_party(self.session, 'Landlord')
        cfa = add_party(self.session, 'Chick-fil-A')
        micro_center = add_party(self.session, 'Micro Center')
        amazon = add_party(self.session, 'Amazon')
        visa = add_party(self.session, 'Visa')
        self.session.commit()

        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 1), party=job, notes='Paycheck #1')
        transfer(self.session, rent, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Rent')
        transfer(self.session, fun, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Fun')
        add_transaction(self.session, fun, Decimal('-7.82'), datetime(2020, 1, 2), party=cfa, notes='Celebrate new job')
        add_transaction(self.session, rent, Decimal('-498'), datetime(2020, 1, 5), party=landlord)
        test = add_transaction(self.session, fun, Decimal('-129.99'), datetime(2020, 1, 10), party=micro_center,
                               notes='New Keyboard')
        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 14), party=job, notes='Paycheck #2')
        transfer(self.session, fun, bank, Decimal('1000'), datetime(2020, 1, 14))
        one = add_transaction(self.session, fun, Decimal('-499.99'), datetime(2020, 1, 18), party=micro_center,
                              notes='New graphics card')
        two = add_transaction(self.session, fun, Decimal('-279.99'), datetime(2020, 1, 19), party=amazon,
                              notes='New processor')
        add_group(self.session, datetime(2020, 2, 1), name='Payment CC091234', party=visa,
                  transactions=[one, two], notes='Paid off credit card')
        self.session.commit()

        account_balances = [
            (fun, Decimal('582.21')),
            (fun, Decimal('862.20')),
            (fun, Decimal('1362.19')),
            (bank, Decimal('0')),
            (bank, Decimal('1000')),
            (fun, Decimal('362.19')),
            (rent, Decimal('2')),
            (fun, Decimal('492.18')),
            (fun, Decimal('500')),
            (bank, Decimal('0')),
            (rent, Decimal('500')),
            (bank, Decimal('500')),
            (bank, Decimal('1000')),
        ]
        transactions = get_transactions(self.session)
        for (balance, transaction) in zip(account_balances, transactions):
            self.assertEqual(balance, (transaction.account, transaction.balance))

        move_transaction(self.session, test, datetime(2020, 1, 18))
        self.session.commit()

        account_balances = [
            (fun, Decimal('582.21')),
            (fun, Decimal('862.20')),
            (fun, Decimal('992.19')),
            (fun, Decimal('1492.18')),
            (bank, Decimal('0')),
            (bank, Decimal('1000')),
            (rent, Decimal('2')),
            (fun, Decimal('492.18')),
            (fun, Decimal('500')),
            (bank, Decimal('0')),
            (rent, Decimal('500')),
            (bank, Decimal('500')),
            (bank, Decimal('1000')),
        ]
        transactions = get_transactions(self.session)
        for (balance, transaction) in zip(account_balances, transactions):
            self.assertEqual(balance, (transaction.account, transaction.balance))

    def test_move_transaction_order(self):
        """Simulates a small sample of a transaction log, then moves a transaction to an exact position
        by editing the date and order.

        Verifies that the transaction changes in the database and is reflected in the surrounding transactions.
        """
        bank = add_account(self.session, 'Bank')
        rent = add_account(self.session, 'Rent')
        fun = add_account(self.session, 'Fun')

        job = add_party(self.session, 'Job')
        landlord = add_party(self.session, 'Landlord')
        cfa = add_party(self.session, 'Chick-fil-A')
        micro_center = add_party(self.session, 'Micro Center')
        amazon = add_party(self.session, 'Amazon')
        visa = add_party(self.session, 'Visa')
        shell = add_party(self.session, 'Shell')
        self.session.commit()

        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 1), party=job, notes='Paycheck #1')
        transfer(self.session, rent, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Rent')
        transfer(self.session, fun, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Fun')
        add_transaction(self.session, fun, Decimal('-7.82'), datetime(2020, 1, 2), party=cfa, notes='Celebrate new job')
        add_transaction(self.session, rent, Decimal('-498'), datetime(2020, 1, 5), party=landlord)
        add_transaction(self.session, fun, Decimal('-20.00'), datetime(2020, 1, 10), party=shell, notes='Gas')
        add_transaction(self.session, fun, Decimal('-24.22'), datetime(2020, 1, 10), party=cfa, notes='Party')
        test = add_transaction(self.session, fun, Decimal('-129.99'), datetime(2020, 1, 10), party=micro_center,
                               notes='New Keyboard')
        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 14), party=job, notes='Paycheck #2')
        transfer(self.session, fun, bank, Decimal('1000'), datetime(2020, 1, 14))
        one = add_transaction(self.session, fun, Decimal('-499.99'), datetime(2020, 1, 18), party=micro_center,
                              notes='New graphics card')
        two = add_transaction(self.session, fun, Decimal('-279.99'), datetime(2020, 1, 19), party=amazon,
                              notes='New processor')
        add_group(self.session, datetime(2020, 2, 1), name='Payment CC091234', party=visa,
                  transactions=[one, two], notes='Paid off credit card')
        self.session.commit()

        account_balances = [
            (fun, Decimal('537.99')),
            (fun, Decimal('817.98')),
            (fun, Decimal('1317.97')),
            (bank, Decimal('0')),
            (bank, Decimal('1000')),
            (fun, Decimal('317.97')),
            (fun, Decimal('447.96')),
            (fun, Decimal('472.18')),
            (rent, Decimal('2')),
            (fun, Decimal('492.18')),
            (fun, Decimal('500')),
            (bank, Decimal('0')),
            (rent, Decimal('500')),
            (bank, Decimal('500')),
            (bank, Decimal('1000')),
        ]
        transactions = get_transactions(self.session)
        for (balance, transaction) in zip(account_balances, transactions):
            self.assertEqual(balance, (transaction.account, transaction.balance))

        move_transaction(self.session, test, datetime(2020, 1, 10), new_order=0)
        self.session.commit()

        account_balances = [
            (fun, Decimal('537.99')),
            (fun, Decimal('817.98')),
            (fun, Decimal('1317.97')),
            (bank, Decimal('0')),
            (bank, Decimal('1000')),
            (fun, Decimal('317.97')),
            (fun, Decimal('342.19')),
            (fun, Decimal('362.19')),
            (rent, Decimal('2')),
            (fun, Decimal('492.18')),
            (fun, Decimal('500')),
            (bank, Decimal('0')),
            (rent, Decimal('500')),
            (bank, Decimal('500')),
            (bank, Decimal('1000')),
        ]
        transactions = get_transactions(self.session)
        for (balance, transaction) in zip(account_balances, transactions):
            self.assertEqual(balance, (transaction.account, transaction.balance))

    def test_move_transaction_edge(self):
        """Simulates a small sample of a transaction log, then moves a transaction to an exact position
        by editing the date and order.

        Verifies that the transaction changes in the database and is reflected in the surrounding transactions.
        """
        bank = add_account(self.session, 'Bank')
        rent = add_account(self.session, 'Rent')
        fun = add_account(self.session, 'Fun')

        job = add_party(self.session, 'Job')
        landlord = add_party(self.session, 'Landlord')
        cfa = add_party(self.session, 'Chick-fil-A')
        micro_center = add_party(self.session, 'Micro Center')
        amazon = add_party(self.session, 'Amazon')
        visa = add_party(self.session, 'Visa')
        shell = add_party(self.session, 'Shell')
        self.session.commit()

        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 1), party=job, notes='Paycheck #1')
        transfer(self.session, rent, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Rent')
        transfer(self.session, fun, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Fun')
        add_transaction(self.session, fun, Decimal('-7.82'), datetime(2020, 1, 2), party=cfa, notes='Celebrate new job')
        add_transaction(self.session, rent, Decimal('-498'), datetime(2020, 1, 5), party=landlord)
        add_transaction(self.session, fun, Decimal('-20.00'), datetime(2020, 1, 10), party=shell, notes='Gas')
        add_transaction(self.session, fun, Decimal('-24.22'), datetime(2020, 1, 10), party=cfa, notes='Party')
        test = add_transaction(self.session, fun, Decimal('-129.99'), datetime(2020, 1, 10), party=micro_center,
                               notes='New Keyboard')
        add_transaction(self.session, fun, Decimal('-10'), datetime(2020, 1, 10), notes='Junk')
        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 14), party=job, notes='Paycheck #2')
        transfer(self.session, fun, bank, Decimal('1000'), datetime(2020, 1, 14))
        one = add_transaction(self.session, fun, Decimal('-499.99'), datetime(2020, 1, 18), party=micro_center,
                              notes='New graphics card')
        two = add_transaction(self.session, fun, Decimal('-279.99'), datetime(2020, 1, 19), party=amazon,
                              notes='New processor')
        add_group(self.session, datetime(2020, 2, 1), name='Payment CC091234', party=visa,
                  transactions=[one, two], notes='Paid off credit card')
        self.session.commit()

        account_balances = [
            (fun, Decimal('527.99')),
            (fun, Decimal('807.98')),
            (fun, Decimal('1307.97')),
            (bank, Decimal('0')),
            (bank, Decimal('1000')),
            (fun, Decimal('307.97')),
            (fun, Decimal('317.97')),
            (fun, Decimal('447.96')),
            (fun, Decimal('472.18')),
            (rent, Decimal('2')),
            (fun, Decimal('492.18')),
            (fun, Decimal('500')),
            (bank, Decimal('0')),
            (rent, Decimal('500')),
            (bank, Decimal('500')),
            (bank, Decimal('1000')),
        ]
        transactions = get_transactions(self.session)
        for (balance, transaction) in zip(account_balances, transactions):
            self.assertEqual(balance, (transaction.account, transaction.balance))

        # Move to the same spot
        move_transaction(self.session, test, datetime(2020, 1, 10), new_order=2)
        self.session.commit()

        account_balances = [
            (fun, Decimal('527.99')),
            (fun, Decimal('807.98')),
            (fun, Decimal('1307.97')),
            (bank, Decimal('0')),
            (bank, Decimal('1000')),
            (fun, Decimal('307.97')),
            (fun, Decimal('317.97')),
            (fun, Decimal('447.96')),
            (fun, Decimal('472.18')),
            (rent, Decimal('2')),
            (fun, Decimal('492.18')),
            (fun, Decimal('500')),
            (bank, Decimal('0')),
            (rent, Decimal('500')),
            (bank, Decimal('500')),
            (bank, Decimal('1000')),
        ]
        transactions = get_transactions(self.session)
        for (balance, transaction) in zip(account_balances, transactions):
            self.assertEqual(balance, (transaction.account, transaction.balance))

        # Move to the top of the day
        move_transaction(self.session, test, datetime(2020, 1, 10))
        self.session.commit()

        account_balances = [
            (fun, Decimal('527.99')),
            (fun, Decimal('807.98')),
            (fun, Decimal('1307.97')),
            (bank, Decimal('0')),
            (bank, Decimal('1000')),
            (fun, Decimal('307.97')),
            (fun, Decimal('437.96')),
            (fun, Decimal('447.96')),
            (fun, Decimal('472.18')),
            (rent, Decimal('2')),
            (fun, Decimal('492.18')),
            (fun, Decimal('500')),
            (bank, Decimal('0')),
            (rent, Decimal('500')),
            (bank, Decimal('500')),
            (bank, Decimal('1000')),
        ]
        transactions = get_transactions(self.session)
        for (balance, transaction) in zip(account_balances, transactions):
            self.assertEqual(balance, (transaction.account, transaction.balance))

        # Move to the bottom of the day
        move_transaction(self.session, test, datetime(2020, 1, 10), new_order=0)
        self.session.commit()

        account_balances = [
            (fun, Decimal('527.99')),
            (fun, Decimal('807.98')),
            (fun, Decimal('1307.97')),
            (bank, Decimal('0')),
            (bank, Decimal('1000')),
            (fun, Decimal('307.97')),
            (fun, Decimal('317.97')),
            (fun, Decimal('342.19')),
            (fun, Decimal('362.19')),
            (rent, Decimal('2')),
            (fun, Decimal('492.18')),
            (fun, Decimal('500')),
            (bank, Decimal('0')),
            (rent, Decimal('500')),
            (bank, Decimal('500')),
            (bank, Decimal('1000')),
        ]
        transactions = get_transactions(self.session)
        for (balance, transaction) in zip(account_balances, transactions):
            self.assertEqual(balance, (transaction.account, transaction.balance))

        # Shift forward one
        move_transaction(self.session, test, datetime(2020, 1, 10), new_order=1)
        self.session.commit()

        account_balances = [
            (fun, Decimal('527.99')),
            (fun, Decimal('807.98')),
            (fun, Decimal('1307.97')),
            (bank, Decimal('0')),
            (bank, Decimal('1000')),
            (fun, Decimal('307.97')),
            (fun, Decimal('317.97')),
            (fun, Decimal('342.19')),
            (fun, Decimal('472.18')),
            (rent, Decimal('2')),
            (fun, Decimal('492.18')),
            (fun, Decimal('500')),
            (bank, Decimal('0')),
            (rent, Decimal('500')),
            (bank, Decimal('500')),
            (bank, Decimal('1000')),
        ]
        transactions = get_transactions(self.session)
        for (balance, transaction) in zip(account_balances, transactions):
            self.assertEqual(balance, (transaction.account, transaction.balance))

        # Shift backward one
        move_transaction(self.session, test, datetime(2020, 1, 10), new_order=0)
        self.session.commit()

        account_balances = [
            (fun, Decimal('527.99')),
            (fun, Decimal('807.98')),
            (fun, Decimal('1307.97')),
            (bank, Decimal('0')),
            (bank, Decimal('1000')),
            (fun, Decimal('307.97')),
            (fun, Decimal('317.97')),
            (fun, Decimal('342.19')),
            (fun, Decimal('362.19')),
            (rent, Decimal('2')),
            (fun, Decimal('492.18')),
            (fun, Decimal('500')),
            (bank, Decimal('0')),
            (rent, Decimal('500')),
            (bank, Decimal('500')),
            (bank, Decimal('1000')),
        ]
        transactions = get_transactions(self.session)
        for (balance, transaction) in zip(account_balances, transactions):
            self.assertEqual(balance, (transaction.account, transaction.balance))

        # Move to the top of the day "manually"
        move_transaction(self.session, test, datetime(2020, 1, 10), new_order=3)
        self.session.commit()

        account_balances = [
            (fun, Decimal('527.99')),
            (fun, Decimal('807.98')),
            (fun, Decimal('1307.97')),
            (bank, Decimal('0')),
            (bank, Decimal('1000')),
            (fun, Decimal('307.97')),
            (fun, Decimal('437.96')),
            (fun, Decimal('447.96')),
            (fun, Decimal('472.18')),
            (rent, Decimal('2')),
            (fun, Decimal('492.18')),
            (fun, Decimal('500')),
            (bank, Decimal('0')),
            (rent, Decimal('500')),
            (bank, Decimal('500')),
            (bank, Decimal('1000')),
        ]
        transactions = get_transactions(self.session)
        for (balance, transaction) in zip(account_balances, transactions):
            self.assertEqual(balance, (transaction.account, transaction.balance))

    def test_edit_group(self):
        """Simulates a small sample of a transaction log, then edits a group's properties.

        Verifies that the group changes in the database and it has the correct properties.
        """
        bank = add_account(self.session, 'Bank')
        rent = add_account(self.session, 'Rent')
        fun = add_account(self.session, 'Fun')

        job = add_party(self.session, 'Job')
        micro_center = add_party(self.session, 'Micro Center')
        amazon = add_party(self.session, 'Amazon')
        visa = add_party(self.session, 'Visa')
        mastercard = add_party(self.session, 'Mastercard')
        self.session.commit()

        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 1), party=job, notes='Paycheck #1')
        transfer(self.session, rent, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Rent')
        transfer(self.session, fun, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Fun')
        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 14), party=job, notes='Paycheck #2')
        transfer(self.session, fun, bank, Decimal('1000'), datetime(2020, 1, 14))
        one = add_transaction(self.session, fun, Decimal('-499.99'), datetime(2020, 1, 18), party=micro_center,
                              notes='New graphics card')
        two = add_transaction(self.session, fun, Decimal('-279.99'), datetime(2020, 1, 19), party=amazon,
                              notes='New processor')
        test = add_group(self.session, datetime(2020, 2, 1), name='Payment CC091234', party=visa,
                         transactions=[one, two], notes='Paid off credit card')
        self.session.commit()

        self.assertEqual(2, len(test.child_transactions))
        self.assertTrue(one in test.child_transactions)
        self.assertTrue(two in test.child_transactions)
        self.assertEqual(0, len(test.child_groups))
        self.assertEqual('Payment CC091234', test.name)
        self.assertEqual(visa, test.party)
        self.assertEqual('Paid off credit card', test.notes)
        self.assertEqual(Decimal('-779.98'), get_group_charge(test))

        edit_group_name(test, 'Payment CC091234 (moved)')
        edit_group_notes(test, 'Paid off credit card (moved)')
        edit_group_party(test, mastercard)
        edit_group_date(test, datetime(2020, 2, 5))
        self.session.commit()

        self.assertEqual(2, len(test.child_transactions))
        self.assertTrue(one in test.child_transactions)
        self.assertTrue(two in test.child_transactions)
        self.assertEqual(0, len(test.child_groups))
        self.assertEqual('Payment CC091234 (moved)', test.name)
        self.assertEqual(mastercard, test.party)
        self.assertEqual('Paid off credit card (moved)', test.notes)
        self.assertEqual(Decimal('-779.98'), get_group_charge(test))

    def test_delete_account(self):
        """Tests deleting an account.

        First creates an account and a transaction using it. Then tries to remove the account and verifies that an
        exception is raised.

        Next the transaction is edited to a different account, then tries to remove the account.
        Verifies that the account be successfully removed.
        """
        bank = add_account(self.session, 'Bank')
        bank_2 = add_account(self.session, 'Bank 2')
        self.session.commit()

        test = get_accounts(self.session)
        self.assertEqual(2, len(test))
        self.assertIn(bank, test)
        self.assertIn(bank_2, test)

        transaction = add_transaction(self.session, bank, Decimal('100'), datetime(2020, 1, 1))

        self.assertRaises(RuntimeError, delete_account, self.session, bank)

        edit_transaction_account(self.session, transaction, bank_2)
        self.session.commit()
        delete_account(self.session, bank)
        self.session.commit()

        test = get_accounts(self.session)
        self.assertEqual(1, len(test))
        self.assertIn(bank_2, test)

    def test_delete_party(self):
        """Tests deleting a party.

        First creates a party and a transaction using it. Then tries to remove the party and verifies that an
        exception is raised.

        Next the transaction is edited to a different party, then tries to remove the account.
        Verifies that the account be successfully removed.
        """
        bank = add_account(self.session, 'Bank')
        rent = add_account(self.session, 'Rent')

        landlord = add_party(self.session, 'Landlord')
        landlady = add_party(self.session, 'Landlady')
        self.session.commit()

        test = get_parties(self.session)
        self.assertEqual(2, len(test))
        self.assertIn(landlord, test)
        self.assertIn(landlady, test)

        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 1))
        transfer(self.session, rent, bank, Decimal('500'), datetime(2020, 1, 1,))
        transaction = add_transaction(self.session, rent, Decimal('498'), datetime(2020, 1, 5), party=landlord)

        self.assertRaises(RuntimeError, delete_party, self.session, landlord)

        edit_transaction_party(transaction, landlady)
        self.session.commit()
        delete_party(self.session, landlord)
        self.session.commit()

        test = get_parties(self.session)
        self.assertEqual(1, len(test))
        self.assertIn(landlady, test)

    def test_delete_transaction(self):
        """Simulates a small sample of a transaction log, then deletes a transaction.

        Verifies that the transaction is removed from the database and is reflected in the future transactions.
        """
        bank = add_account(self.session, 'Bank')
        rent = add_account(self.session, 'Rent')
        fun = add_account(self.session, 'Fun')

        job = add_party(self.session, 'Job')
        landlord = add_party(self.session, 'Landlord')
        cfa = add_party(self.session, 'Chick-fil-A')
        micro_center = add_party(self.session, 'Micro Center')
        amazon = add_party(self.session, 'Amazon')
        visa = add_party(self.session, 'Visa')
        self.session.commit()

        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 1), party=job, notes='Paycheck #1')
        transfer(self.session, rent, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Rent')
        transfer(self.session, fun, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Fun')
        add_transaction(self.session, fun, Decimal('-7.82'), datetime(2020, 1, 2), party=cfa, notes='Celebrate new job')
        add_transaction(self.session, rent, Decimal('-498'), datetime(2020, 1, 5), party=landlord)
        test = add_transaction(self.session, fun, Decimal('-129.99'), datetime(2020, 1, 10), party=micro_center,
                               notes='New Keyboard')
        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 14), party=job, notes='Paycheck #2')
        transfer(self.session, fun, bank, Decimal('1000'), datetime(2020, 1, 14))
        one = add_transaction(self.session, fun, Decimal('-499.99'), datetime(2020, 1, 18), party=micro_center,
                              notes='New graphics card')
        two = add_transaction(self.session, fun, Decimal('-279.99'), datetime(2020, 1, 19), party=amazon,
                              notes='New processor')
        add_group(self.session, datetime(2020, 2, 1), name='Payment CC091234', party=visa,
                  transactions=[one, two], notes='Paid off credit card')
        self.session.commit()

        account_balances = [
            (fun, Decimal('582.21')),
            (fun, Decimal('862.20')),
            (fun, Decimal('1362.19')),
            (bank, Decimal('0')),
            (bank, Decimal('1000')),
            (fun, Decimal('362.19')),
            (rent, Decimal('2')),
            (fun, Decimal('492.18')),
            (fun, Decimal('500')),
            (bank, Decimal('0')),
            (rent, Decimal('500')),
            (bank, Decimal('500')),
            (bank, Decimal('1000')),
        ]
        transactions = get_transactions(self.session)
        for (balance, transaction) in zip(account_balances, transactions):
            self.assertEqual(balance, (transaction.account, transaction.balance))

        delete_transaction(self.session, test)
        self.session.commit()

        account_balances = [
            (fun, Decimal('712.20')),
            (fun, Decimal('992.19')),
            (fun, Decimal('1492.18')),
            (bank, Decimal('0')),
            (bank, Decimal('1000')),
            (rent, Decimal('2')),
            (fun, Decimal('492.18')),
            (fun, Decimal('500')),
            (bank, Decimal('0')),
            (rent, Decimal('500')),
            (bank, Decimal('500')),
            (bank, Decimal('1000')),
        ]
        transactions = get_transactions(self.session)
        for (balance, transaction) in zip(account_balances, transactions):
            self.assertEqual(balance, (transaction.account, transaction.balance))

    def test_ungroup(self):
        """Simulates a small sample of a transaction log with a group, then ungroups the group.

        Verifies that the transaction history is unchanged, but the group is gone.
        """
        bank = add_account(self.session, 'Bank')
        rent = add_account(self.session, 'Rent')
        fun = add_account(self.session, 'Fun')
        dining = add_account(self.session, 'Dining')

        job = add_party(self.session, 'Job')
        landlord = add_party(self.session, 'Landlord')
        cfa = add_party(self.session, 'Chick-fil-A')
        visa = add_party(self.session, 'Visa')
        self.session.commit()

        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 1), party=job, notes='Paycheck #1')
        transfer(self.session, rent, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Rent')
        transfer(self.session, fun, bank, Decimal('400'), datetime(2020, 1, 1), notes='Budget for Fun')
        transfer(self.session, dining, bank, Decimal('100'), datetime(2020, 1, 1), notes='Budget for Dining')
        one = add_transaction(self.session, rent, Decimal('-498'), datetime(2020, 1, 5), party=landlord)
        two = add_transaction(self.session, dining, Decimal('-7.89'), datetime(2020, 1, 7), party=cfa, notes='Meal')
        three = add_transaction(self.session, fun, Decimal('-2.99'), datetime(2020, 1, 7), party=cfa, notes='Dessert')
        group_1 = add_group(self.session, datetime(2020, 1, 7), name='Out to CFA', party=cfa,
                            transactions=[two, three], notes='Test 1')
        group_2 = add_group(self.session, datetime(2020, 1, 15), name='Auto-pay', party=visa,
                            sub_groups=[group_1], transactions=[one], notes='Test 2')
        add_transaction(self.session, fun, Decimal('-12.34'), datetime(2020, 1, 19), party=cfa, notes='Whoops')
        self.session.commit()

        test = get_groups(self.session)
        self.assertEqual(5, len(test))
        self.assertIn(group_1, test)
        self.assertIn(group_2, test)

        account_balances = [
            (fun, Decimal('384.67')),
            (fun, Decimal('397.01')),
            (dining, Decimal('92.11')),
            (rent, Decimal('2')),
            (dining, Decimal('100')),
            (bank, Decimal('0')),
            (fun, Decimal('400')),
            (bank, Decimal('100')),
            (rent, Decimal('500')),
            (bank, Decimal('500')),
            (bank, Decimal('1000')),
        ]
        transactions = get_transactions(self.session)
        for (balance, transaction) in zip(account_balances, transactions):
            self.assertEqual(balance, (transaction.account, transaction.balance))

        ungroup(self.session, group_2)
        self.session.commit()

        transactions = get_transactions(self.session)
        for (balance, transaction) in zip(account_balances, transactions):
            self.assertEqual(balance, (transaction.account, transaction.balance))

        test = get_groups(self.session)
        self.assertEqual(4, len(test))
        self.assertIn(group_1, test)
        self.assertNotIn(group_2, test)

    def test_delete_group(self):
        """Simulates a small sample of a transaction log, then deletes a group.

        Verifies that the group and all transactions are removed from the database and is reflected in the
        future transactions.
        """
        bank = add_account(self.session, 'Bank')
        rent = add_account(self.session, 'Rent')
        fun = add_account(self.session, 'Fun')
        dining = add_account(self.session, 'Dining')

        job = add_party(self.session, 'Job')
        landlord = add_party(self.session, 'Landlord')
        cfa = add_party(self.session, 'Chick-fil-A')
        visa = add_party(self.session, 'Visa')
        self.session.commit()

        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 1), party=job, notes='Paycheck #1')
        transfer(self.session, rent, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Rent')
        transfer(self.session, fun, bank, Decimal('400'), datetime(2020, 1, 1), notes='Budget for Fun')
        transfer(self.session, dining, bank, Decimal('100'), datetime(2020, 1, 1), notes='Budget for Dining')
        one = add_transaction(self.session, rent, Decimal('-498'), datetime(2020, 1, 5), party=landlord)
        two = add_transaction(self.session, dining, Decimal('-7.89'), datetime(2020, 1, 7), party=cfa, notes='Meal')
        three = add_transaction(self.session, fun, Decimal('-2.99'), datetime(2020, 1, 7), party=cfa, notes='Dessert')
        group_1 = add_group(self.session, datetime(2020, 1, 7), name='Out to CFA', party=cfa,
                            transactions=[two, three], notes='Test 1')
        group_2 = add_group(self.session, datetime(2020, 1, 15), name='Auto-pay', party=visa,
                            sub_groups=[group_1], transactions=[one], notes='Test 2')
        add_transaction(self.session, fun, Decimal('-12.34'), datetime(2020, 1, 19), party=cfa, notes='Whoops')
        self.session.commit()

        test = get_groups(self.session)
        self.assertEqual(5, len(test))
        self.assertIn(group_1, test)
        self.assertIn(group_2, test)

        account_balances = [
            (fun, Decimal('384.67')),
            (fun, Decimal('397.01')),
            (dining, Decimal('92.11')),
            (rent, Decimal('2')),
            (dining, Decimal('100')),
            (bank, Decimal('0')),
            (fun, Decimal('400')),
            (bank, Decimal('100')),
            (rent, Decimal('500')),
            (bank, Decimal('500')),
            (bank, Decimal('1000')),
        ]
        transactions = get_transactions(self.session)
        for (balance, transaction) in zip(account_balances, transactions):
            self.assertEqual(balance, (transaction.account, transaction.balance))

        delete_group(self.session, group_2)
        self.session.commit()

        account_balances = [
            (fun, Decimal('387.66')),
            (dining, Decimal('100')),
            (bank, Decimal('0')),
            (fun, Decimal('400')),
            (bank, Decimal('100')),
            (rent, Decimal('500')),
            (bank, Decimal('500')),
            (bank, Decimal('1000')),
        ]
        transactions = get_transactions(self.session)
        for (balance, transaction) in zip(account_balances, transactions):
            self.assertEqual(balance, (transaction.account, transaction.balance))

        test = get_groups(self.session)
        self.assertEqual(3, len(test))
        self.assertNotIn(group_1, test)
        self.assertNotIn(group_2, test)

    def test_add_tag(self):
        """Adds a tag. Verifies that the tag appears in the database and the properties are correct."""
        tag = add_tag(self.session, 'test')
        self.session.commit()
        self.assertEqual('test', tag.name)
        test = get_tag_by_id(self.session, tag.id)
        self.assertEqual(tag, test)

    def test_edit_tag(self):
        """Edits a tag, then verifies that the tag is updated in the database."""
        tag = add_tag(self.session, 'test')
        self.session.commit()
        self.assertEqual('test', tag.name)

        edit_tag(tag, 'new')
        self.session.commit()

        self.assertEqual('new', tag.name)
        test = get_tag_by_id(self.session, tag.id)
        self.assertEqual('new', test.name)

    def test_delete_tag(self):
        """Tests deleting a tag.

        First creates a tag and a transaction using it. Then tries to remove the tag and verifies that an
        exception is raised.

        Next the transaction is edited to remove the tag, then tries to remove the tag.
        Verifies that the tag is successfully removed.
        """
        tag_1 = add_tag(self.session, 'tag 1')
        tag_2 = add_tag(self.session, 'tag 2')

        bank = add_account(self.session, 'Bank')
        rent = add_account(self.session, 'Rent')
        fun = add_account(self.session, 'Fun')
        dining = add_account(self.session, 'Dining')

        job = add_party(self.session, 'Job')
        landlord = add_party(self.session, 'Landlord')
        cfa = add_party(self.session, 'Chick-fil-A')
        visa = add_party(self.session, 'Visa')
        amazon = add_party(self.session, 'Amazon')
        self.session.commit()

        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 1), party=job, notes='Paycheck #1')
        transfer(self.session, rent, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Rent')
        transfer(self.session, fun, bank, Decimal('400'), datetime(2020, 1, 1), notes='Budget for Fun')
        transfer(self.session, dining, bank, Decimal('100'), datetime(2020, 1, 1), notes='Budget for Dining')
        one = add_transaction(self.session, rent, Decimal('-498'), datetime(2020, 1, 5), party=landlord)
        two = add_transaction(self.session, fun, Decimal('-279.99'), datetime(2020, 1, 19), party=amazon,
                              notes='New processor')
        group_1 = add_group(self.session, datetime(2020, 2, 1), name='Payment CC091234', party=visa,
                  transactions=[one, two], notes='Paid off credit card')

        add_tag_to_transaction(self.session, one, tag_1)
        add_tag_to_transaction(self.session, one, tag_2)
        self.session.commit()

        self.assertRaises(RuntimeError, delete_tag, self.session, tag_1)

        remove_tag_from_transaction(self.session, one, tag_1)
        self.session.commit()

        delete_tag(self.session, tag_1)
        self.session.commit()

        tags = get_transaction_tags(one)
        self.assertEqual(1, len(tags))
        self.assertIn(tag_2, tags)

        add_tag_to_group(self.session, group_1, tag_2)
        self.session.commit()

        self.assertRaises(RuntimeError, delete_tag, self.session, tag_2)

        remove_tag_from_transaction(self.session, one, tag_2)
        self.session.commit()

        self.assertRaises(RuntimeError, delete_tag, self.session, tag_2)

        remove_tag_from_group(self.session, group_1, tag_2)
        self.session.commit()

        delete_tag(self.session, tag_2)
        self.session.commit()

        tags = get_group_tags(group_1)
        self.assertEqual(0, len(tags))

    def test_add_tag_to_transaction(self):
        """Simulates a small sample of a transaction log, then adds tags to several transactions.

        Verifies that the transactions are tagged in the database.
        """
        tag_1 = add_tag(self.session, 'tag 1')
        tag_2 = add_tag(self.session, 'tag 2')

        bank = add_account(self.session, 'Bank')
        rent = add_account(self.session, 'Rent')
        fun = add_account(self.session, 'Fun')
        dining = add_account(self.session, 'Dining')

        job = add_party(self.session, 'Job')
        landlord = add_party(self.session, 'Landlord')
        cfa = add_party(self.session, 'Chick-fil-A')
        visa = add_party(self.session, 'Visa')
        self.session.commit()

        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 1), party=job, notes='Paycheck #1')
        transfer(self.session, rent, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Rent')
        transfer(self.session, fun, bank, Decimal('400'), datetime(2020, 1, 1), notes='Budget for Fun')
        transfer(self.session, dining, bank, Decimal('100'), datetime(2020, 1, 1), notes='Budget for Dining')
        one = add_transaction(self.session, rent, Decimal('-498'), datetime(2020, 1, 5), party=landlord)

        add_tag_to_transaction(self.session, one, tag_1)
        add_tag_to_transaction(self.session, one, tag_2)
        self.session.commit()

        tags = get_transaction_tags(one)
        self.assertEqual(2, len(tags))
        self.assertIn(tag_1, tags)
        self.assertIn(tag_2, tags)

        tag_maps = get_tag_maps(self.session)
        self.assertEqual(2, len(tag_maps))

    def test_remove_tag_from_transaction(self):
        """Simulates a small sample of a transaction log, then adds tags to several transactions.

        Verifies that the transactions are tagged in the database.

        Removes the tags from a transaction and verifies that the transaction is no longer tagged in the database.
        """
        tag_1 = add_tag(self.session, 'tag 1')
        tag_2 = add_tag(self.session, 'tag 2')

        bank = add_account(self.session, 'Bank')
        rent = add_account(self.session, 'Rent')
        fun = add_account(self.session, 'Fun')
        dining = add_account(self.session, 'Dining')

        job = add_party(self.session, 'Job')
        landlord = add_party(self.session, 'Landlord')
        cfa = add_party(self.session, 'Chick-fil-A')
        visa = add_party(self.session, 'Visa')
        self.session.commit()

        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 1), party=job, notes='Paycheck #1')
        transfer(self.session, rent, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Rent')
        transfer(self.session, fun, bank, Decimal('400'), datetime(2020, 1, 1), notes='Budget for Fun')
        transfer(self.session, dining, bank, Decimal('100'), datetime(2020, 1, 1), notes='Budget for Dining')
        one = add_transaction(self.session, rent, Decimal('-498'), datetime(2020, 1, 5), party=landlord)

        add_tag_to_transaction(self.session, one, tag_1)
        add_tag_to_transaction(self.session, one, tag_2)
        self.session.commit()

        tags = get_transaction_tags(one)
        self.assertEqual(2, len(tags))
        self.assertIn(tag_1, tags)
        self.assertIn(tag_2, tags)

        tag_maps = get_tag_maps(self.session)
        self.assertEqual(2, len(tag_maps))

        remove_tag_from_transaction(self.session, one, tag_1)
        self.session.commit()

        tags = get_transaction_tags(one)
        self.assertEqual(1, len(tags))
        self.assertIn(tag_2, tags)

        tag_maps = get_tag_maps(self.session)
        self.assertEqual(1, len(tag_maps))

        # This shouldn't cause any errors
        remove_tag_from_transaction(self.session, one, tag_1)
        self.session.commit()

        tags = get_transaction_tags(one)
        self.assertEqual(1, len(tags))
        self.assertIn(tag_2, tags)

        tag_maps = get_tag_maps(self.session)
        self.assertEqual(1, len(tag_maps))

        remove_tag_from_transaction(self.session, one, tag_2)
        self.session.commit()

        tags = get_transaction_tags(one)
        self.assertEqual(0, len(tags))

        tag_maps = get_tag_maps(self.session)
        self.assertEqual(0, len(tag_maps))

    def test_delete_transaction_with_tags(self):
        """Simulates a small sample of a transaction log, then adds tags to several transactions.

        Deletes a transaction with tags and verifies that the transaction no longer appears in the tag map.
        """
        tag_1 = add_tag(self.session, 'tag 1')
        tag_2 = add_tag(self.session, 'tag 2')

        bank = add_account(self.session, 'Bank')
        rent = add_account(self.session, 'Rent')
        fun = add_account(self.session, 'Fun')
        dining = add_account(self.session, 'Dining')

        job = add_party(self.session, 'Job')
        landlord = add_party(self.session, 'Landlord')
        cfa = add_party(self.session, 'Chick-fil-A')
        visa = add_party(self.session, 'Visa')
        self.session.commit()

        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 1), party=job, notes='Paycheck #1')
        transfer(self.session, rent, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Rent')
        transfer(self.session, fun, bank, Decimal('400'), datetime(2020, 1, 1), notes='Budget for Fun')
        transfer(self.session, dining, bank, Decimal('100'), datetime(2020, 1, 1), notes='Budget for Dining')
        one = add_transaction(self.session, rent, Decimal('-498'), datetime(2020, 1, 5), party=landlord)

        add_tag_to_transaction(self.session, one, tag_1)
        add_tag_to_transaction(self.session, one, tag_2)
        self.session.commit()

        tag_maps = get_tag_maps(self.session)
        self.assertEqual(2, len(tag_maps))

        delete_transaction(self.session, one)
        self.session.commit()

        tag_maps = get_tag_maps(self.session)
        self.assertEqual(0, len(tag_maps))

    def test_move_transaction_with_tags(self):
        """Simulates a small sample of a transaction log, adds tags to a transaction, then moves the transaction.

        Verifies that the transaction retains all tags after being moved."""
        tag_1 = add_tag(self.session, 'tag 1')
        tag_2 = add_tag(self.session, 'tag 2')

        bank = add_account(self.session, 'Bank')
        rent = add_account(self.session, 'Rent')
        fun = add_account(self.session, 'Fun')

        job = add_party(self.session, 'Job')
        landlord = add_party(self.session, 'Landlord')
        cfa = add_party(self.session, 'Chick-fil-A')
        micro_center = add_party(self.session, 'Micro Center')
        amazon = add_party(self.session, 'Amazon')
        visa = add_party(self.session, 'Visa')
        self.session.commit()

        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 1), party=job, notes='Paycheck #1')
        transfer(self.session, rent, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Rent')
        transfer(self.session, fun, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Fun')
        add_transaction(self.session, fun, Decimal('-7.82'), datetime(2020, 1, 2), party=cfa, notes='Celebrate new job')
        add_transaction(self.session, rent, Decimal('-498'), datetime(2020, 1, 5), party=landlord)
        test = add_transaction(self.session, fun, Decimal('-129.99'), datetime(2020, 1, 10), party=micro_center,
                               notes='New Keyboard')
        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 14), party=job, notes='Paycheck #2')
        transfer(self.session, fun, bank, Decimal('1000'), datetime(2020, 1, 14))
        one = add_transaction(self.session, fun, Decimal('-499.99'), datetime(2020, 1, 18), party=micro_center,
                              notes='New graphics card')
        two = add_transaction(self.session, fun, Decimal('-279.99'), datetime(2020, 1, 19), party=amazon,
                              notes='New processor')
        add_group(self.session, datetime(2020, 2, 1), name='Payment CC091234', party=visa,
                  transactions=[one, two], notes='Paid off credit card')
        add_tag_to_transaction(self.session, test, tag_1)
        add_tag_to_transaction(self.session, test, tag_2)
        self.session.commit()

        tags = get_transaction_tags(test)
        self.assertEqual(2, len(tags))
        self.assertIn(tag_1, tags)
        self.assertIn(tag_2, tags)

        tag_maps = get_tag_maps(self.session)
        self.assertEqual(2, len(tag_maps))

        move_transaction(self.session, test, datetime(2020, 1, 18))
        self.session.commit()

        tags = get_transaction_tags(test)
        self.assertEqual(2, len(tags))
        self.assertIn(tag_1, tags)
        self.assertIn(tag_2, tags)

        tag_maps = get_tag_maps(self.session)
        self.assertEqual(2, len(tag_maps))

    def test_add_tag_to_group(self):
        """Simulates a small sample of a transaction log, then adds tags to a group.

        Verifies that the group is tagged in the database.
        """
        tag_1 = add_tag(self.session, 'tag 1')
        tag_2 = add_tag(self.session, 'tag 2')

        bank = add_account(self.session, 'Bank')
        rent = add_account(self.session, 'Rent')
        fun = add_account(self.session, 'Fun')
        dining = add_account(self.session, 'Dining')

        job = add_party(self.session, 'Job')
        landlord = add_party(self.session, 'Landlord')
        cfa = add_party(self.session, 'Chick-fil-A')
        visa = add_party(self.session, 'Visa')
        self.session.commit()

        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 1), party=job, notes='Paycheck #1')
        transfer(self.session, rent, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Rent')
        transfer(self.session, fun, bank, Decimal('400'), datetime(2020, 1, 1), notes='Budget for Fun')
        transfer(self.session, dining, bank, Decimal('100'), datetime(2020, 1, 1), notes='Budget for Dining')
        one = add_transaction(self.session, rent, Decimal('-498'), datetime(2020, 1, 5), party=landlord)
        two = add_transaction(self.session, dining, Decimal('-7.89'), datetime(2020, 1, 7), party=cfa, notes='Meal')
        three = add_transaction(self.session, fun, Decimal('-2.99'), datetime(2020, 1, 7), party=cfa, notes='Dessert')
        group_1 = add_group(self.session, datetime(2020, 1, 7), name='Out to CFA', party=cfa,
                            transactions=[two, three], notes='Test 1')
        group_2 = add_group(self.session, datetime(2020, 1, 15), name='Auto-pay', party=visa,
                            sub_groups=[group_1], transactions=[one], notes='Test 2')
        add_transaction(self.session, fun, Decimal('-12.34'), datetime(2020, 1, 19), party=cfa, notes='Whoops')
        self.session.commit()

        add_tag_to_group(self.session, group_1, tag_1)
        add_tag_to_group(self.session, group_1, tag_2)
        self.session.commit()

        tags = get_group_tags(group_1)
        self.assertEqual(2, len(tags))
        self.assertIn(tag_1, tags)
        self.assertIn(tag_2, tags)

        tag_maps = get_tag_group_maps(self.session)
        self.assertEqual(2, len(tag_maps))

    def test_remove_tag_from_group(self):
        """Simulates a small sample of a transaction log, then adds tags to a group.

        Verifies that the group is tagged in the database.

        Removes the tags from the group and verifies that the group is no longer tagged in the database.
        """
        tag_1 = add_tag(self.session, 'tag 1')
        tag_2 = add_tag(self.session, 'tag 2')

        bank = add_account(self.session, 'Bank')
        rent = add_account(self.session, 'Rent')
        fun = add_account(self.session, 'Fun')
        dining = add_account(self.session, 'Dining')

        job = add_party(self.session, 'Job')
        landlord = add_party(self.session, 'Landlord')
        cfa = add_party(self.session, 'Chick-fil-A')
        visa = add_party(self.session, 'Visa')
        self.session.commit()

        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 1), party=job, notes='Paycheck #1')
        transfer(self.session, rent, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Rent')
        transfer(self.session, fun, bank, Decimal('400'), datetime(2020, 1, 1), notes='Budget for Fun')
        transfer(self.session, dining, bank, Decimal('100'), datetime(2020, 1, 1), notes='Budget for Dining')
        one = add_transaction(self.session, rent, Decimal('-498'), datetime(2020, 1, 5), party=landlord)
        two = add_transaction(self.session, dining, Decimal('-7.89'), datetime(2020, 1, 7), party=cfa, notes='Meal')
        three = add_transaction(self.session, fun, Decimal('-2.99'), datetime(2020, 1, 7), party=cfa, notes='Dessert')
        group_1 = add_group(self.session, datetime(2020, 1, 7), name='Out to CFA', party=cfa,
                            transactions=[two, three], notes='Test 1')
        group_2 = add_group(self.session, datetime(2020, 1, 15), name='Auto-pay', party=visa,
                            sub_groups=[group_1], transactions=[one], notes='Test 2')
        add_transaction(self.session, fun, Decimal('-12.34'), datetime(2020, 1, 19), party=cfa, notes='Whoops')
        self.session.commit()

        add_tag_to_group(self.session, group_1, tag_1)
        add_tag_to_group(self.session, group_1, tag_2)
        self.session.commit()

        tags = get_group_tags(group_1)
        self.assertEqual(2, len(tags))
        self.assertIn(tag_1, tags)
        self.assertIn(tag_2, tags)

        tag_maps = get_tag_group_maps(self.session)
        self.assertEqual(2, len(tag_maps))

        remove_tag_from_group(self.session, group_1, tag_1)
        self.session.commit()

        tags = get_group_tags(group_1)
        self.assertEqual(1, len(tags))
        self.assertIn(tag_2, tags)

        tag_maps = get_tag_group_maps(self.session)
        self.assertEqual(1, len(tag_maps))

        # This shouldn't break anything
        remove_tag_from_group(self.session, group_1, tag_1)
        self.session.commit()

        tags = get_group_tags(group_1)
        self.assertEqual(1, len(tags))
        self.assertIn(tag_2, tags)

        tag_maps = get_tag_group_maps(self.session)
        self.assertEqual(1, len(tag_maps))

        remove_tag_from_group(self.session, group_1, tag_2)
        self.session.commit()

        tags = get_group_tags(group_1)
        self.assertEqual(0, len(tags))

        tag_maps = get_tag_group_maps(self.session)
        self.assertEqual(0, len(tag_maps))

    def test_ungroup_group_with_tags(self):
        """Simulates a small sample of a transaction log, then adds tags to a group.

        Verifies that the group is tagged in the database.

        Ungroups the group and verifies that the group is no longer tagged in the database.
        """
        tag_1 = add_tag(self.session, 'tag 1')
        tag_2 = add_tag(self.session, 'tag 2')

        bank = add_account(self.session, 'Bank')
        rent = add_account(self.session, 'Rent')
        fun = add_account(self.session, 'Fun')
        dining = add_account(self.session, 'Dining')

        job = add_party(self.session, 'Job')
        landlord = add_party(self.session, 'Landlord')
        cfa = add_party(self.session, 'Chick-fil-A')
        visa = add_party(self.session, 'Visa')
        self.session.commit()

        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 1), party=job, notes='Paycheck #1')
        transfer(self.session, rent, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Rent')
        transfer(self.session, fun, bank, Decimal('400'), datetime(2020, 1, 1), notes='Budget for Fun')
        transfer(self.session, dining, bank, Decimal('100'), datetime(2020, 1, 1), notes='Budget for Dining')
        one = add_transaction(self.session, rent, Decimal('-498'), datetime(2020, 1, 5), party=landlord)
        two = add_transaction(self.session, dining, Decimal('-7.89'), datetime(2020, 1, 7), party=cfa, notes='Meal')
        three = add_transaction(self.session, fun, Decimal('-2.99'), datetime(2020, 1, 7), party=cfa, notes='Dessert')
        group_1 = add_group(self.session, datetime(2020, 1, 7), name='Out to CFA', party=cfa,
                            transactions=[two, three], notes='Test 1')
        group_2 = add_group(self.session, datetime(2020, 1, 15), name='Auto-pay', party=visa,
                            sub_groups=[group_1], transactions=[one], notes='Test 2')
        add_transaction(self.session, fun, Decimal('-12.34'), datetime(2020, 1, 19), party=cfa, notes='Whoops')
        self.session.commit()

        add_tag_to_group(self.session, group_1, tag_1)
        add_tag_to_group(self.session, group_1, tag_2)
        self.session.commit()

        tags = get_group_tags(group_1)
        self.assertEqual(2, len(tags))
        self.assertIn(tag_1, tags)
        self.assertIn(tag_2, tags)

        tag_maps = get_tag_group_maps(self.session)
        self.assertEqual(2, len(tag_maps))

        ungroup(self.session, group_1)
        self.session.commit()

        tag_maps = get_tag_group_maps(self.session)
        self.assertEqual(0, len(tag_maps))

    def test_delete_group_with_tags(self):
        """Simulates a small sample of a transaction log, then adds tags a group.

        Deletes the group and verifies that the group no longer appears in the tag map.
        """
        tag_1 = add_tag(self.session, 'tag 1')
        tag_2 = add_tag(self.session, 'tag 2')

        bank = add_account(self.session, 'Bank')
        rent = add_account(self.session, 'Rent')
        fun = add_account(self.session, 'Fun')
        dining = add_account(self.session, 'Dining')

        job = add_party(self.session, 'Job')
        landlord = add_party(self.session, 'Landlord')
        cfa = add_party(self.session, 'Chick-fil-A')
        visa = add_party(self.session, 'Visa')
        self.session.commit()

        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 1), party=job, notes='Paycheck #1')
        transfer(self.session, rent, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Rent')
        transfer(self.session, fun, bank, Decimal('400'), datetime(2020, 1, 1), notes='Budget for Fun')
        transfer(self.session, dining, bank, Decimal('100'), datetime(2020, 1, 1), notes='Budget for Dining')
        one = add_transaction(self.session, rent, Decimal('-498'), datetime(2020, 1, 5), party=landlord)
        two = add_transaction(self.session, dining, Decimal('-7.89'), datetime(2020, 1, 7), party=cfa, notes='Meal')
        three = add_transaction(self.session, fun, Decimal('-2.99'), datetime(2020, 1, 7), party=cfa, notes='Dessert')
        group_1 = add_group(self.session, datetime(2020, 1, 7), name='Out to CFA', party=cfa,
                            transactions=[two, three], notes='Test 1')
        group_2 = add_group(self.session, datetime(2020, 1, 15), name='Auto-pay', party=visa,
                            sub_groups=[group_1], transactions=[one], notes='Test 2')
        add_transaction(self.session, fun, Decimal('-12.34'), datetime(2020, 1, 19), party=cfa, notes='Whoops')
        self.session.commit()

        add_tag_to_group(self.session, group_1, tag_1)
        add_tag_to_group(self.session, group_1, tag_2)
        self.session.commit()

        tags = get_group_tags(group_1)
        self.assertEqual(2, len(tags))
        self.assertIn(tag_1, tags)
        self.assertIn(tag_2, tags)

        tag_maps = get_tag_group_maps(self.session)
        self.assertEqual(2, len(tag_maps))

        delete_group(self.session, group_1)
        self.session.commit()

        tag_maps = get_tag_group_maps(self.session)
        self.assertEqual(0, len(tag_maps))

    def test_get_transaction_by_tag(self):
        """Simulates a small sample of a transaction log included tagged transactions.

        Verifies that the correction transactions can be found when searching by tag.
        """
        tag_1 = add_tag(self.session, 'tag 1')
        tag_2 = add_tag(self.session, 'tag 2')
        tag_3 = add_tag(self.session, 'tag 3')
        tag_4 = add_tag(self.session, 'tag 4')
        tag_5 = add_tag(self.session, 'tag 5')
        tag_q = add_tag(self.session, 'tag q')

        bank = add_account(self.session, 'Bank')
        rent = add_account(self.session, 'Rent')
        fun = add_account(self.session, 'Fun')
        dining = add_account(self.session, 'Dining')

        job = add_party(self.session, 'Job')
        landlord = add_party(self.session, 'Landlord')
        cfa = add_party(self.session, 'Chick-fil-A')
        visa = add_party(self.session, 'Visa')
        self.session.commit()

        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 1), party=job, notes='Paycheck #1')
        transfer(self.session, rent, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Rent')
        transfer(self.session, fun, bank, Decimal('400'), datetime(2020, 1, 1), notes='Budget for Fun')
        transfer(self.session, dining, bank, Decimal('100'), datetime(2020, 1, 1), notes='Budget for Dining')
        add_transaction(self.session, rent, Decimal('-498'), datetime(2020, 1, 5), party=landlord)
        a = add_transaction(self.session, rent, Decimal('-1'), datetime(2020, 1, 5))
        b = add_transaction(self.session, rent, Decimal('-2'), datetime(2020, 1, 5))
        c = add_transaction(self.session, rent, Decimal('-3'), datetime(2020, 1, 5))
        d = add_transaction(self.session, rent, Decimal('-4'), datetime(2020, 1, 5))
        e = add_transaction(self.session, rent, Decimal('-5'), datetime(2020, 1, 5))
        f = add_transaction(self.session, rent, Decimal('-10'), datetime(2020, 1, 5))
        g = add_transaction(self.session, rent, Decimal('-20'), datetime(2020, 1, 5))
        h = add_transaction(self.session, rent, Decimal('-30'), datetime(2020, 1, 5))
        i = add_transaction(self.session, rent, Decimal('-40'), datetime(2020, 1, 5))
        j = add_transaction(self.session, rent, Decimal('-50'), datetime(2020, 1, 5))

        add_tag_to_transaction(self.session, a, tag_1)
        add_tag_to_transaction(self.session, b, tag_2)
        add_tag_to_transaction(self.session, c, tag_3)
        add_tag_to_transaction(self.session, d, tag_4)
        add_tag_to_transaction(self.session, e, tag_5)
        add_tag_to_transaction(self.session, f, tag_1)
        add_tag_to_transaction(self.session, g, tag_2)
        add_tag_to_transaction(self.session, h, tag_3)
        add_tag_to_transaction(self.session, i, tag_4)
        add_tag_to_transaction(self.session, j, tag_5)

        add_tag_to_transaction(self.session, j, tag_q)
        self.session.commit()

        test = get_transactions(self.session, tags=[tag_4])
        self.assertEqual(2, len(test))
        self.assertIn(d, test)
        self.assertIn(i, test)

        test = get_transactions(self.session, tags=[tag_4, tag_q])
        self.assertEqual(0, len(test))

        test = get_transactions(self.session, tags=[tag_5, tag_q])
        self.assertEqual(1, len(test))
        self.assertIn(j, test)

    def test_get_transaction_by_tag_and_charge(self):
        """Simulates a small sample of a transaction log included tagged transactions.

        Verifies that the correction transactions can be found when searching by tag and charge.
        """
        tag_1 = add_tag(self.session, 'tag 1')
        tag_2 = add_tag(self.session, 'tag 2')
        tag_3 = add_tag(self.session, 'tag 3')
        tag_4 = add_tag(self.session, 'tag 4')
        tag_5 = add_tag(self.session, 'tag 5')
        tag_q = add_tag(self.session, 'tag q')

        bank = add_account(self.session, 'Bank')
        rent = add_account(self.session, 'Rent')
        fun = add_account(self.session, 'Fun')
        dining = add_account(self.session, 'Dining')

        job = add_party(self.session, 'Job')
        landlord = add_party(self.session, 'Landlord')
        cfa = add_party(self.session, 'Chick-fil-A')
        visa = add_party(self.session, 'Visa')
        self.session.commit()

        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 1), party=job, notes='Paycheck #1')
        transfer(self.session, rent, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Rent')
        transfer(self.session, fun, bank, Decimal('400'), datetime(2020, 1, 1), notes='Budget for Fun')
        transfer(self.session, dining, bank, Decimal('100'), datetime(2020, 1, 1), notes='Budget for Dining')
        add_transaction(self.session, rent, Decimal('-498'), datetime(2020, 1, 5), party=landlord)
        a = add_transaction(self.session, rent, Decimal('-1'), datetime(2020, 1, 5))
        b = add_transaction(self.session, rent, Decimal('-2'), datetime(2020, 1, 5))
        c = add_transaction(self.session, rent, Decimal('-3'), datetime(2020, 1, 5))
        d = add_transaction(self.session, rent, Decimal('-4'), datetime(2020, 1, 5))
        e = add_transaction(self.session, rent, Decimal('-5'), datetime(2020, 1, 5))
        f = add_transaction(self.session, rent, Decimal('-10'), datetime(2020, 1, 5))
        g = add_transaction(self.session, rent, Decimal('-20'), datetime(2020, 1, 5))
        h = add_transaction(self.session, rent, Decimal('-30'), datetime(2020, 1, 5))
        i = add_transaction(self.session, rent, Decimal('-40'), datetime(2020, 1, 5))
        j = add_transaction(self.session, rent, Decimal('-50'), datetime(2020, 1, 5))

        add_tag_to_transaction(self.session, a, tag_1)
        add_tag_to_transaction(self.session, b, tag_1)
        add_tag_to_transaction(self.session, c, tag_1)
        add_tag_to_transaction(self.session, d, tag_1)
        add_tag_to_transaction(self.session, e, tag_1)
        add_tag_to_transaction(self.session, f, tag_2)
        add_tag_to_transaction(self.session, g, tag_2)
        add_tag_to_transaction(self.session, h, tag_2)
        add_tag_to_transaction(self.session, i, tag_2)
        add_tag_to_transaction(self.session, j, tag_2)

        add_tag_to_transaction(self.session, i, tag_q)
        add_tag_to_transaction(self.session, j, tag_q)
        self.session.commit()

        test = get_transactions(self.session, charge_begin=Decimal('3'), tags=[tag_1])
        self.assertEqual(3, len(test))
        self.assertIn(c, test)
        self.assertIn(d, test)
        self.assertIn(e, test)

        test = get_transactions(self.session, charge_begin=Decimal('45'), tags=[tag_2, tag_q])
        self.assertEqual(1, len(test))
        self.assertIn(j, test)

        test = get_transactions(self.session, charge_begin=Decimal('60'), tags=[tag_2, tag_q])
        self.assertEqual(0, len(test))

    def test_get_group_by_tag(self):
        tag_1 = add_tag(self.session, 'tag 1')
        tag_2 = add_tag(self.session, 'tag 2')
        tag_3 = add_tag(self.session, 'tag 3')

        bank = add_account(self.session, 'Bank')
        rent = add_account(self.session, 'Rent')
        fun = add_account(self.session, 'Fun')
        dining = add_account(self.session, 'Dining')

        job = add_party(self.session, 'Job')
        landlord = add_party(self.session, 'Landlord')
        cfa = add_party(self.session, 'Chick-fil-A')
        visa = add_party(self.session, 'Visa')
        self.session.commit()

        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 1), party=job, notes='Paycheck #1')
        transfer(self.session, rent, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Rent')
        transfer(self.session, fun, bank, Decimal('400'), datetime(2020, 1, 1), notes='Budget for Fun')
        transfer(self.session, dining, bank, Decimal('100'), datetime(2020, 1, 1), notes='Budget for Dining')
        one = add_transaction(self.session, rent, Decimal('-498'), datetime(2020, 1, 5), party=landlord)
        two = add_transaction(self.session, dining, Decimal('-7.89'), datetime(2020, 1, 7), party=cfa, notes='Meal')
        three = add_transaction(self.session, fun, Decimal('-2.99'), datetime(2020, 1, 7), party=cfa, notes='Dessert')
        group_1 = add_group(self.session, datetime(2020, 1, 7), name='Out to CFA', party=cfa,
                            transactions=[two, three], notes='Test 1')
        group_2 = add_group(self.session, datetime(2020, 1, 15), name='Auto-pay', party=visa,
                            sub_groups=[group_1], transactions=[one], notes='Test 2')
        add_transaction(self.session, fun, Decimal('-12.34'), datetime(2020, 1, 19), party=cfa, notes='Whoops')
        self.session.commit()

        add_tag_to_group(self.session, group_1, tag_1)
        add_tag_to_group(self.session, group_1, tag_2)
        add_tag_to_group(self.session, group_2, tag_1)
        add_tag_to_group(self.session, group_2, tag_3)
        self.session.commit()

        test = get_groups(self.session, tags=[tag_1])
        self.assertEqual(2, len(test))
        self.assertIn(group_1, test)
        self.assertIn(group_2, test)

        test = get_groups(self.session, tags=[tag_1, tag_2])
        self.assertEqual(1, len(test))
        self.assertIn(group_1, test)

        test = get_groups(self.session, tags=[tag_1, tag_2, tag_3])
        self.assertEqual(0, len(test))

    def test_get_group_by_tag_and_charge(self):
        tag_1 = add_tag(self.session, 'tag 1')
        tag_2 = add_tag(self.session, 'tag 2')

        bank = add_account(self.session, 'Bank')
        rent = add_account(self.session, 'Rent')
        fun = add_account(self.session, 'Fun')
        dining = add_account(self.session, 'Dining')

        job = add_party(self.session, 'Job')
        landlord = add_party(self.session, 'Landlord')
        cfa = add_party(self.session, 'Chick-fil-A')
        visa = add_party(self.session, 'Visa')
        self.session.commit()

        add_transaction(self.session, bank, Decimal('1000'), datetime(2020, 1, 1), party=job, notes='Paycheck #1')
        transfer(self.session, rent, bank, Decimal('500'), datetime(2020, 1, 1), notes='Budget for Rent')
        transfer(self.session, fun, bank, Decimal('400'), datetime(2020, 1, 1), notes='Budget for Fun')
        transfer(self.session, dining, bank, Decimal('100'), datetime(2020, 1, 1), notes='Budget for Dining')
        one = add_transaction(self.session, rent, Decimal('-498'), datetime(2020, 1, 5), party=landlord)
        two = add_transaction(self.session, dining, Decimal('-7.89'), datetime(2020, 1, 7), party=cfa, notes='Meal')
        three = add_transaction(self.session, fun, Decimal('-2.99'), datetime(2020, 1, 7), party=cfa, notes='Dessert')
        group_1 = add_group(self.session, datetime(2020, 1, 7), name='Out to CFA', party=cfa,
                            transactions=[two, three], notes='Test 1')
        group_2 = add_group(self.session, datetime(2020, 1, 15), name='Auto-pay', party=visa,
                            sub_groups=[group_1], transactions=[one], notes='Test 2')
        add_transaction(self.session, fun, Decimal('-12.34'), datetime(2020, 1, 19), party=cfa, notes='Whoops')
        four = add_transaction(self.session, dining, Decimal('23.99'), datetime(2020, 1, 23), party=cfa, notes='Meal')
        five = add_transaction(self.session, fun, Decimal('4.99'), datetime(2020, 1, 23), party=cfa, notes='Dessert')
        group_3 = add_group(self.session, datetime(2020, 1, 23), name='Out to CFA', party=cfa,
                            transactions=[four, five], notes='Test 3')
        self.session.commit()

        add_tag_to_group(self.session, group_1, tag_1)
        add_tag_to_group(self.session, group_1, tag_2)
        add_tag_to_group(self.session, group_2, tag_1)
        add_tag_to_group(self.session, group_2, tag_2)
        add_tag_to_group(self.session, group_3, tag_1)
        add_tag_to_group(self.session, group_3, tag_2)
        self.session.commit()

        test = get_groups(self.session, charge_end=Decimal('400'), tags=[tag_1, tag_2])
        self.assertEqual(2, len(test))
        self.assertIn(group_1, test)
        self.assertIn(group_3, test)


if __name__ == '__main__':
    unittest.main()
