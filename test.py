from decimal import Decimal
from datetime import datetime

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from budget.database import Base

from budget.database.action import *

import sys


if __name__ == '__main__':
    engine = create_engine('postgres://smcfalls:password@127.0.0.1/budget', echo=True)
    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)

    Session = sessionmaker(bind=engine)
    session = Session()

    bank = add_account(session, 'Bank')
    sam = add_account(session, 'Sam Allowance')
    amanda = add_account(session, 'Amanda Allowance')
    session.commit()

    cfa = add_party(session, 'Chick-fil-A')
    newegg = add_party(session, 'newegg')
    microcenter = add_party(session, 'Microcenter')
    capital_one = add_party(session, 'Capital One')
    ts = add_party(session, 'Transformational Security')
    verizon = add_party(session, 'Verizon')
    session.commit()

    add_transaction(session, bank, Decimal('2319.35'), datetime(2020, 9, 30), party=ts, notes='Paycheck V28291 #101')
    add_transaction(session, bank, Decimal('-150'), datetime(2020, 9, 30), notes='Sam Allowance')
    add_transaction(session, sam, Decimal('+150'), datetime(2020, 9, 30), notes='Sam Allowance')
    add_transaction(session, bank, Decimal('-150'), datetime(2020, 9, 30), notes='Amanda Allowance')
    add_transaction(session, amanda, Decimal('+150'), datetime(2020, 9, 30), notes='Amanda Allowance')
    add_transaction(session, bank, Decimal('2319.35'), datetime(2020, 10, 13), party=ts, notes='Paycheck V28291 #102')
    add_transaction(session, bank, Decimal('2319.35'), datetime(2020, 10, 27), party=ts, notes='Paycheck V28291 #103')
    add_transaction(session, bank, Decimal('2319.35'), datetime(2020, 9, 16), party=ts, notes='(Forgot) Paycheck V28291 #100')
    edit = add_transaction(session, sam, Decimal('-19.99'), datetime(2020, 11, 1), party=newegg, notes='edit me')
    add_transaction(session, sam, Decimal('-39.99'), datetime(2020, 11, 2), party=newegg, notes='toys')
    edit2 = add_transaction(session, amanda, Decimal('-40'), datetime(2020, 11, 1), party=capital_one)
    add_transaction(session, amanda, Decimal('-12.94'), datetime(2020, 11, 15), party=cfa)
    add_transaction(session, bank, Decimal('-39.99'), datetime(2020, 11, 16), party=verizon, notes='Internet Bill')
    session.commit()

    edit_transaction_charge(session, edit, Decimal('-14.99'))
    session.commit()

    edit_transaction_account(session, edit2, bank)
    session.commit()

    add_transaction(session, amanda, Decimal('-12.44'), datetime(2020, 11, 18), party=cfa)
    add_transaction(session, amanda, Decimal('-12.43'), datetime(2020, 11, 18), party=cfa)
    add_transaction(session, amanda, Decimal('-12.42'), datetime(2020, 11, 18), party=cfa)
    add_transaction(session, amanda, Decimal('-30'), datetime(2020, 11, 18), party=cfa, order=1)
    session.commit()

    del1 = add_transaction(session, bank, Decimal('-150'), datetime(2020, 11, 30), notes='Sam Allowance')
    del2 = add_transaction(session, sam, Decimal('+150'), datetime(2020, 11, 30), notes='Sam Allowance')
    add_transaction(session, bank, Decimal('-150'), datetime(2020, 11, 30), notes='Amanda Allowance')
    add_transaction(session, amanda, Decimal('+150'), datetime(2020, 11, 30), notes='Amanda Allowance')
    session.commit()

    delete_transaction(session, del1)
    delete_transaction(session, del2)
    session.commit()

    add_transaction(session, bank, Decimal('-1'), datetime(2020, 12, 1))
    add_transaction(session, bank, Decimal('-2'), datetime(2020, 12, 1))
    mov3 = add_transaction(session, bank, Decimal('-3'), datetime(2020, 12, 1))
    add_transaction(session, bank, Decimal('-4'), datetime(2020, 12, 2))
    add_transaction(session, bank, Decimal('-5'), datetime(2020, 12, 2))
    mov6 = add_transaction(session, bank, Decimal('-6'), datetime(2020, 12, 2))
    add_transaction(session, bank, Decimal('-7'), datetime(2020, 12, 3))
    add_transaction(session, bank, Decimal('-8'), datetime(2020, 12, 3))
    session.commit()

    move_transaction(session, mov3, datetime(2020, 12, 1), 0)
    move_transaction(session, mov6, datetime(2020, 12, 3), 1)
    session.commit()

    # add_account(session, '👍🏻 One')
    # add_account(session, '👍 Two')
    # session.commit()

    print(get_account_balance(session, bank))
    print(get_account_balance(session, sam))
    print(get_account_balance(session, amanda))
    transactions = session.query(Transaction).order_by(Transaction.date.desc(), Transaction.order.desc()).all()

    find_account = find_account_by_name(session, '%a%')
    print(find_account)

    print(get_transactions(session, charge_end=Decimal('200'), debit=True))

